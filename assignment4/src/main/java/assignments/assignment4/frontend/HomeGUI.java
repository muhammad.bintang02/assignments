package assignments.assignment4.frontend;

import javax.swing.JFrame;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    JLabel titleLabel;
    JButton button1, button2, button3, button4, button5, button6;
    JPanel mainPanel, btnPanel, ttlPanel;
    GridBagConstraints gbc = new GridBagConstraints();  // Objek GridBagConstraints untuk mengatur peletakan komponen di GridBagLayout
    Color buttonColor = new Color(72,125,195);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);
        
        // TODO: Implementasikan Halaman Home

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);
        
        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);

        // Panel untuk meletakkan titleLabel
        ttlPanel = new JPanel(new GridBagLayout());
        ttlPanel.setBackground(panelColor);
        ttlPanel.add(titleLabel);

        // Panel untuk meletakkan button-button
        btnPanel = new JPanel(new GridBagLayout());
        btnPanel.setBackground(panelColor);

        // Membuat tombol "Tambah Mahasiswa"
        button1 = new JButton("Tambah Mahasiswa");
        button1.setFont(SistemAkademikGUI.fontGeneral); // Set font
        button1.setBackground(buttonColor); // Set warna tombol
        button1.setForeground(fontColor);   // Set warna tulisan
        button1.setBorderPainted(false);    // Hapus border button
        button1.setFocusable(false);    // Membuat button tidak focusable
        
        // Set lokasi button1 
        gbc.gridx = 0;
        gbc.gridy = 0;
        btnPanel.add(button1, gbc); // Menambahkan button ke btnPanel sesuai dengan posisi gbc yang diset

        // Membuat tombol "Tambah Mata Kuliah"
        button2 = new JButton("Tambah Mata Kuliah");
        button2.setFont(SistemAkademikGUI.fontGeneral); 
        button2.setBackground(buttonColor); 
        button2.setForeground(fontColor);   
        button2.setBorderPainted(false);
        button2.setFocusable(false);
        
        // Karena button disusun secara vertikal, untuk button selanjutnya hanya perlu mengganti nilai gridy (gridx tetap 0)
        gbc.gridy = 1;
        btnPanel.add(button2, gbc);

        // Membuat tombol "Tambah IRS"
        button3 = new JButton("Tambah IRS");
        button3.setFont(SistemAkademikGUI.fontGeneral);
        button3.setBackground(buttonColor);
        button3.setForeground(fontColor);
        button3.setBorderPainted(false);
        button3.setFocusable(false);
        gbc.gridy = 2;
        btnPanel.add(button3, gbc);

        // Membuat tombol "Hapus IRS"
        button4 = new JButton("Hapus IRS");
        button4.setFont(SistemAkademikGUI.fontGeneral);
        button4.setBackground(buttonColor);
        button4.setForeground(fontColor);
        button4.setBorderPainted(false);
        button4.setFocusable(false);
        gbc.gridy = 3;
        btnPanel.add(button4, gbc);

        // Membuat tombol "Lihat Ringkasan Mahasiswa"
        button5 = new JButton("Lihat Ringkasan Mahasiswa");
        button5.setFont(SistemAkademikGUI.fontGeneral);
        button5.setBackground(buttonColor);
        button5.setForeground(fontColor);
        button5.setBorderPainted(false);
        button5.setFocusable(false);
        gbc.gridy = 4;
        btnPanel.add(button5, gbc);

        // Membuat tombol "Lihat Ringkasan Mata Kuliah"
        button6 = new JButton("Lihat Ringkasan Mata Kuliah");
        button6.setFont(SistemAkademikGUI.fontGeneral);
        button6.setBackground(buttonColor);
        button6.setForeground(fontColor);
        button6.setBorderPainted(false);
        button6.setFocusable(false);
        gbc.gridy = 5;
        btnPanel.add(button6, gbc);
        
        gbc.gridy = 0;  // Set lokasi ttlPanel
        mainPanel.add(ttlPanel, gbc);   // Menambahkan ttlPanel ke mainPanel sesuai dengan posisi gbc yang diset
        
        gbc.gridy = 1;  // Set lokasi btnPanel
        mainPanel.add(btnPanel, gbc);   // Menambahkan btnPanel ke mainPanel sesuai dengan posisi gbc yang diset

        frame.add(mainPanel);   // Menambahkan mainPanel ke frame

        // Menambahkan action listener ke button "Tambah Mahasiswa" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Hapus semua komponen di content pane
                frame.getContentPane().removeAll();     

                // Inisialisasi objek TambahMahasiswaGUI
                new TambahMahasiswaGUI(frame,daftarMahasiswa,daftarMataKuliah); 
                
                // Memanggil repaint() dan revalidate() agar frame menampilkan komponen baru yang ditambahkan oleh objek TambahMahasiswa GUI
                frame.repaint();
                frame.revalidate();
            }
        });

        // Menambahkan action listener ke button "Tambah Mata Kuliah" (button2)
        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new TambahMataKuliahGUI(frame,daftarMahasiswa,daftarMataKuliah);    // Inisialisasi objek TambahMataKuliahGUI
                frame.repaint();
                frame.revalidate();
            }
        });

        // Menambahkan action listener ke button "Tambah IRS" (button3)
        button3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new TambahIRSGUI(frame,daftarMahasiswa,daftarMataKuliah);   // Inisialisasi objek TambahIRSGUI
                frame.repaint();
                frame.revalidate();
            }
        });

        // Menambahkan action listener ke button "Hapus IRS" (button4)
        button4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HapusIRSGUI(frame,daftarMahasiswa,daftarMataKuliah);    // Inisialisasi objek HapusIRSGUI
                frame.repaint();
                frame.revalidate();
            }
        });

        // Menambahkan action listener ke button "Lihat Ringkasan Mahasiswa" (button5)
        button5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new RingkasanMahasiswaGUI(frame,daftarMahasiswa,daftarMataKuliah); // Inisialisasi objek RingkasanMahasiswaGUI
                frame.repaint();
                frame.revalidate();
            }
        });

        // Menambahkan action listener ke button "Lihat Ringkasan Mata Kuliah" (button6)
        button6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new RingkasanMataKuliahGUI(frame,daftarMahasiswa,daftarMataKuliah); // Inisialisasi objek RingkasanMataKuliahGUI
                frame.repaint();
                frame.revalidate();
            }
        });
    }
}
