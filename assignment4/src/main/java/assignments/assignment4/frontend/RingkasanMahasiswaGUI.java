package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {
    ArrayList<Long> listNpm = new ArrayList<Long>();
    JLabel titleLabel, label1;
    JButton button1, button2;
    JComboBox<Long> box1;
    JPanel mainPanel, btnPanel, ttlPanel, cbPanel;
    GridBagConstraints gbc = new GridBagConstraints();
    Color button1Color = new Color(72,125,195);
    Color button2Color = new Color(156,198,52);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mahasiswa

        // Sort arraylist daftarMahasiswa agar objek terurut dari npm terkecil
        sortNpm(daftarMahasiswa);

        // Membuat listNpm yang isinya npm yang sudah terurut
        createListNpm(daftarMahasiswa);

        titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);
        
        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);

        // Panel untuk meletakkan titleLabel
        ttlPanel = new JPanel(new GridBagLayout());
        ttlPanel.setBackground(panelColor);
        ttlPanel.add(titleLabel);

        // Panel untuk meletakkan combobox
        cbPanel = new JPanel(new GridBagLayout());
        cbPanel.setBackground(panelColor);

        // Membuat label "Pilih NPM"
        label1 = new JLabel("Pilih NPM");
        label1.setFont(SistemAkademikGUI.fontGeneral);
        label1.setForeground(fontColor);
        gbc.gridx = 0;
        gbc.gridy = 0;
        cbPanel.add(label1, gbc);

        // Membuat combobox yang menampilakan npm yang sudah terurut
        box1 = new JComboBox<Long>(listNpm.toArray(new Long[0]));
        box1.setPreferredSize(new Dimension(125,23));
        gbc.gridy = 1;
        cbPanel.add(box1, gbc);

        // Panel untuk meletakkan button-button
        btnPanel = new JPanel(new GridBagLayout());
        btnPanel.setBackground(panelColor);

        // Membuat button "Lihat"
        button1 = new JButton("Lihat");
        button1.setFont(SistemAkademikGUI.fontGeneral);
        button1.setBackground(button1Color);
        button1.setForeground(fontColor);
        button1.setBorderPainted(false);
        button1.setFocusable(false);
        gbc.gridy = 0;
        btnPanel.add(button1, gbc);

        // Membuat button "Kembali"
        button2 = new JButton("Kembali");
        button2.setFont(SistemAkademikGUI.fontGeneral);
        button2.setBackground(button2Color);
        button2.setForeground(fontColor);
        button2.setBorderPainted(false);
        button2.setFocusable(false);
        gbc.gridy = 1;
        btnPanel.add(button2, gbc);

        gbc.gridy = 0;
        mainPanel.add(ttlPanel, gbc);
        
        gbc.gridy = 1;
        mainPanel.add(cbPanel,gbc);

        gbc.gridy = 2;
        mainPanel.add(btnPanel,gbc);
        
        frame.add(mainPanel);

        // Menambahkan action listener ke button "Lihat" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                // Jika masih ada field yang kosong, tampilkan pesan berikut
                if(box1.getSelectedItem() == null) {
                    String pesan = "Mohon isi seluruh field";
                    JOptionPane.showMessageDialog(null, pesan);
                
                // Jika field sudah terisi, kode ini akan dijalankan
                } else {
                    // Mendapatkan data yang dipilih
                    long npm = (long) box1.getSelectedItem();

                    // Mendapatkan objek mahasiswa berdasarkan npm
                    Mahasiswa mahasiswa = getMahasiswa(daftarMahasiswa, npm);
                
                    frame.getContentPane().removeAll(); 
                    new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah); // Inisialisasi objek DetailRingkasanMahasiswaGUI
                    frame.setSize(500,700); // Perbesar ukuran frame    
                    frame.setLocationRelativeTo(null);
                    frame.repaint();    
                    frame.revalidate();
                }
            }
        });

        // Menambahkan action listener ke button "Kembali" (button2)
        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HomeGUI(frame,daftarMahasiswa,daftarMataKuliah);
                frame.repaint();
                frame.revalidate();
            }
        });
    }

    private Mahasiswa getMahasiswa(ArrayList<Mahasiswa> daftarMahasiswa, long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }

    private void createListNpm(ArrayList<Mahasiswa> daftarMahasiswa) {
        for(Mahasiswa mahasiswa : daftarMahasiswa) {
            listNpm.add(mahasiswa.getNpm());
        }
    }

    private void sortNpm(ArrayList<Mahasiswa> daftarMahasiswa) {
        for(int i = 0; i < daftarMahasiswa.size(); i++) {
            for(int j = 0; j < daftarMahasiswa.size() - 1; j++) {
                if(daftarMahasiswa.get(j).getNpm() > daftarMahasiswa.get(j+1).getNpm()) {
                    Mahasiswa tmp = daftarMahasiswa.get(j);
                    daftarMahasiswa.set(j, daftarMahasiswa.get(j+1));
                    daftarMahasiswa.set(j+1, tmp);
                }
            }
        }
    }
}
