package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{
    JLabel titleLabel, label1, label2;
    JButton button1, button2;
    JTextField field1, field2;
    JPanel mainPanel, btnPanel, ttlPanel, fldPanel;
    GridBagConstraints gbc = new GridBagConstraints();  // Objek GridBagConstraints untuk mengatur peletakan komponen di GridBagLayout
    Color button1Color = new Color(72,125,195);
    Color button2Color = new Color(156,198,52);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        // TODO: Implementasikan Tambah Mahasiswa
        titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);

        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);

        // Panel untuk meletakkan titleLabel
        ttlPanel = new JPanel(new GridBagLayout());
        ttlPanel.setBackground(panelColor);
        ttlPanel.add(titleLabel);

        // Panel untuk meletakkan field dan label yang bersesuaian
        fldPanel = new JPanel(new GridBagLayout());
        fldPanel.setBackground(panelColor);

        // Membuat label "Nama:"
        label1 = new JLabel("Nama:");
        label1.setFont(SistemAkademikGUI.fontGeneral);
        label1.setForeground(fontColor);
        
        // Set lokasi label
        gbc.gridx = 0;
        gbc.gridy = 0;
        fldPanel.add(label1, gbc);  // Menambahkan label ke fldPanel sesuai dengan posisi gbc yang diset

        // Membuat text field yang akan menampung nama yang diinput user
        field1 = new JTextField();
        field1.setPreferredSize(new Dimension(225,23)); // Set ukuran text field
        
        // Karena komponen disusun secara vertikal, untuk komponen selanjutnya hanya perlu mengganti nilai gridy (gridx tetap 0)
        gbc.gridy = 1;
        fldPanel.add(field1, gbc);  // Menambahkan field ke fldPanel sesuai dengan posisi gbc yang diset

        // Membuat label "NPM:"
        label2 = new JLabel("NPM:");
        label2.setFont(SistemAkademikGUI.fontGeneral);
        label2.setForeground(fontColor);
        gbc.gridy = 2;
        fldPanel.add(label2, gbc);

        // Membuat text field yang akan menampung npm yang diinput user
        field2 = new JTextField();
        field2.setPreferredSize(new Dimension(225,23));
        gbc.gridy = 3;
        fldPanel.add(field2, gbc);

        // Panel untuk meletakkan button-button
        btnPanel = new JPanel(new GridBagLayout());
        btnPanel.setBackground(panelColor);

        // Membuat button "Tambahkan"
        button1 = new JButton("Tambahkan");
        button1.setFont(SistemAkademikGUI.fontGeneral);
        button1.setBackground(button1Color);
        button1.setForeground(fontColor);
        button1.setBorderPainted(false);
        button1.setFocusable(false);
        gbc.gridy = 0;
        btnPanel.add(button1, gbc);

        // Membuat button "Kembali"
        button2 = new JButton("Kembali");
        button2.setFont(SistemAkademikGUI.fontGeneral);
        button2.setBackground(button2Color);
        button2.setForeground(fontColor);
        button2.setBorderPainted(false);
        button2.setFocusable(false);
        gbc.gridy = 1;
        btnPanel.add(button2, gbc);

        gbc.gridy = 0;  // Set lokasi ttlPanel
        mainPanel.add(ttlPanel, gbc);   // Menambahkan ttlPanel ke mainPanel sesuai dengan posisi gbc yang diset
        
        gbc.gridy = 1;  // Set lokasi fldPanel
        mainPanel.add(fldPanel,gbc);   // Menambahkan fldPanel ke mainPanel sesuai dengan posisi gbc yang diset

        gbc.gridy = 2;  // Set lokasi btnPanel
        mainPanel.add(btnPanel,gbc);    // Menambahkan btnPanel ke mainPanel sesuai dengan posisi gbc yang diset
        
        frame.add(mainPanel);   // Menambahkan mainPanel ke frame

        // Menambahkan action listener ke button "Tambahkan" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                // Jika masih ada field yang kosong, tampilkan pesan berikut
                if(field1.getText().isEmpty() | field2.getText().isEmpty()) {
                    String pesan = "Mohon isi seluruh field";
                    JOptionPane.showMessageDialog(null, pesan);  
                
                // Jika kedua field sudah terisi, kode ini akan dijalankan
                } else {
                    String nama = field1.getText(); // Mendapatkan nama yang dimasukkan user
                    long npm = Long.parseLong(field2.getText());    // Mendapatkan npm yang dimasukkan user

                    // Jika npm sudah terdaftar, kode berikut akan dijalankan
                    if(cekNPM(daftarMahasiswa, npm)) {  // Npm dicek menggunakan method cekNPM
                        
                        // Kosongkan kedua field
                        field1.setText("");
                        field2.setText("");

                        // Tampilkan pesan berikut
                        String pesan = String.format("NPM %d sudah pernah ditambahkan sebelumnya", npm);
                        JOptionPane.showMessageDialog(null, pesan);
                    
                    // Jika belum terdaftar, kode ini akan dijalankan
                    } else {
                        
                        // Membuat objek mahasiswa baru dengan atribut yang dimasukkan oleh user dan menambahkannya ke arraylist daftarMahasiswa
                        daftarMahasiswa.add(new Mahasiswa(nama, npm));
                        
                        // Kosongkan kedua field
                        field1.setText("");
                        field2.setText("");

                        // Tampilkan pesan berikut
                        String pesan = String.format("Mahasiswa %d-%s berhasil ditambakan", npm, nama);
                        JOptionPane.showMessageDialog(null, pesan);
                    }
                }
                
            }
        });
        
        // Menambahkan action listener ke button "Kembali" (button2)
        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HomeGUI(frame,daftarMahasiswa,daftarMataKuliah);    // Inisialisasi objek HomeGUI
                frame.repaint();
                frame.revalidate();
            }
        });
    }

    // Method untuk mengecek apakah npm sudah terdaftar atau belum
    private boolean cekNPM(ArrayList<Mahasiswa> daftarMahasiswa, long npm) {
        for(Mahasiswa element : daftarMahasiswa) {
            if(element.getNpm() == npm) {   
                return true;    // Jika terdapat mahasiswa yang memiliki npm yang sama dengan parameter akan direturn true
            }
        }
        return false;
    }
    
}
