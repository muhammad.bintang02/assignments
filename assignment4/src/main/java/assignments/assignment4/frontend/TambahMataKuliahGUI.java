package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{
    JLabel titleLabel, label1, label2, label3, label4;
    JButton button1, button2, button3, button4;
    JTextField field1, field2, field3, field4;
    JPanel mainPanel, btnPanel, ttlPanel, fldPanel;
    GridBagConstraints gbc = new GridBagConstraints();
    Color button1Color = new Color(72,125,195);
    Color button2Color = new Color(156,198,52);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // TODO: Implementasikan Tambah Mata Kuliah
        titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);
        
        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);

        // Panel untuk meletakkan titleLabel
        ttlPanel = new JPanel(new GridBagLayout());
        ttlPanel.setBackground(panelColor);
        ttlPanel.add(titleLabel);

        // Panel untuk meletakkan field dan label yang bersesuaian
        fldPanel = new JPanel(new GridBagLayout());
        fldPanel.setBackground(panelColor);

        // Membuat label "Kode Mata Kuliah:"
        label1 = new JLabel("Kode Mata Kuliah:");
        label1.setFont(SistemAkademikGUI.fontGeneral);
        label1.setForeground(fontColor);
        gbc.gridx = 0;
        gbc.gridy = 0;
        fldPanel.add(label1, gbc);

        // Membuat field yang akan menampung kode mata kuliah
        field1 = new JTextField();
        field1.setPreferredSize(new Dimension(225,23));
        gbc.gridy = 1;
        fldPanel.add(field1, gbc);

        // Membuat label "Nama Mata Kuliah: "
        label2 = new JLabel("Nama Mata Kuliah: ");
        label2.setFont(SistemAkademikGUI.fontGeneral);
        label2.setForeground(fontColor);
        gbc.gridy = 2;
        fldPanel.add(label2, gbc);

        // Membuat field yang akan menampung nama matkul
        field2 = new JTextField();
        field2.setPreferredSize(new Dimension(225,23));
        gbc.gridy = 3;
        fldPanel.add(field2, gbc);

        // Membuat label "SKS:"
        label3 = new JLabel("SKS: ");
        label3.setFont(SistemAkademikGUI.fontGeneral);
        label3.setForeground(fontColor);
        gbc.gridy = 4;
        fldPanel.add(label3, gbc);

        // Membuat field yang akan menampung SKS
        field3 = new JTextField();
        field3.setPreferredSize(new Dimension(225,23));
        gbc.gridy = 5;
        fldPanel.add(field3, gbc);

        // Membuat label "Kapasitas"
        label4 = new JLabel("Kapasitas: ");
        label4.setFont(SistemAkademikGUI.fontGeneral);
        label4.setForeground(fontColor);
        gbc.gridy = 6;
        fldPanel.add(label4, gbc);

        // Membuat field yang akan menampung kapasitas
        field4 = new JTextField();
        field4.setPreferredSize(new Dimension(225,23));
        gbc.gridy = 7;
        fldPanel.add(field4, gbc);

        // Panel untuk meletakkan button-button2
        btnPanel = new JPanel(new GridBagLayout());
        btnPanel.setBackground(panelColor);

        // Membuat button "Tambahkan"
        button1 = new JButton("Tambahkan");
        button1.setFont(SistemAkademikGUI.fontGeneral);
        button1.setBackground(button1Color);
        button1.setForeground(fontColor);
        button1.setBorderPainted(false);
        button1.setFocusable(false);
        gbc.gridy = 0;
        btnPanel.add(button1, gbc);

        // Membuat button "Kembali"
        button2 = new JButton("Kembali");
        button2.setFont(SistemAkademikGUI.fontGeneral);
        button2.setBackground(button2Color);
        button2.setForeground(fontColor);
        button2.setBorderPainted(false);
        button2.setFocusable(false);
        gbc.gridy = 1;
        btnPanel.add(button2, gbc);
        
        gbc.gridy = 0;
        mainPanel.add(ttlPanel, gbc);

        gbc.gridy = 1;
        mainPanel.add(fldPanel,gbc);

        gbc.gridy = 2;
        mainPanel.add(btnPanel,gbc);
        
        frame.add(mainPanel);

        // Menambahkan action listener ke button "Tambahkan" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                
                // Jika masih ada field yang kosong, tampilkan pesan berikut
                if(field1.getText().isEmpty() | field2.getText().isEmpty() 
                    | field3.getText().isEmpty() | field4.getText().isEmpty()) {
                    String pesan = "Mohon isi seluruh field";
                    JOptionPane.showMessageDialog(null, pesan);  
                
                // Jika kedua field sudah terisi, kode ini akan dijalankan
                } else {
                    
                    // Mendapatkan input yang dimasukkan oleh user di masing2 field
                    String kode = field1.getText();
                    String nama = field2.getText();
                    int sks = Integer.parseInt(field3.getText());
                    int kapasitas = Integer.parseInt(field4.getText());

                    // Jika matkul sudah pernah ditambahkan, kode berikut akan dijalankan
                    if(cekMatkul(daftarMataKuliah, nama)) { // Matkul dicek menggunakan method cekMatkul
                        
                        // Kosongkan field
                        field1.setText("");
                        field2.setText("");
                        field3.setText("");
                        field4.setText("");

                        // Tampilkan pesan berikut
                        String pesan = String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama);
                        JOptionPane.showMessageDialog(null, pesan);
                    
                    // Jika belum pernah, kode berikut akan dijalankan
                    } else {
                        
                        // Membuat objek matakuliah baru dengan atribut yang dimasukkan oleh user dan menambahkannya ke arraylist daftarMataKuliah
                        daftarMataKuliah.add(new MataKuliah(kode, nama, sks, kapasitas));
                        
                        // Kosongkan field
                        field1.setText("");
                        field2.setText("");
                        field3.setText("");
                        field4.setText("");
                        
                        // Tampilakan pesan berikut
                        String pesan = String.format("Mata Kuliah %s berhasil ditambakan",  nama);
                        JOptionPane.showMessageDialog(null, pesan);
                    }
                }
                
            }
        });

        // Menambahkan action listener ke button "Kembali" (button2)
        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HomeGUI(frame,daftarMahasiswa,daftarMataKuliah);    // Inisialisasi objek HomeGUI
                frame.repaint();
                frame.revalidate();
            }
        });
    }

    // Method untuk mengecek apakah matkul sudah terdaftar atau belum
    private boolean cekMatkul(ArrayList<MataKuliah> daftarMataKuliah, String nama) {
        for(MataKuliah element : daftarMataKuliah) {
            if(element.getNama().equals(nama)) {
                return true;    // Jika terdapat matakuliah yang namanya sama dengan parameter akan direturn true
            }
        }
        return false;
    }
    
}
