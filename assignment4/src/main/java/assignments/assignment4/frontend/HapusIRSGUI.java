package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {
    ArrayList<Long> listNpm = new ArrayList<Long>();    // arraylist yang akan menampung npm mahasiswa
    ArrayList<String> listMatkul = new ArrayList<String>(); // arraylist yang akan menampung nama matkul
    JLabel titleLabel, label1, label2;
    JButton button1, button2;
    JComboBox<Long> box1;
    JComboBox<String> box2;
    JPanel mainPanel, btnPanel, ttlPanel, cbPanel;
    GridBagConstraints gbc = new GridBagConstraints();
    Color button1Color = new Color(72,125,195);
    Color button2Color = new Color(156,198,52);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Hapus IRS

        // Sort arraylist daftarMahasiswa agar objek terurut dari npm terkecil
        sortNpm(daftarMahasiswa);

        // Sort arraylist daftarMataKuliah agar objek terurut secara alfabet
        sortMatkul(daftarMataKuliah);
        
        // Membuat listNpm dan listMatkul yang isinya npm dan matkul yang sudah terurut
        createListNpmAndMatkul(daftarMahasiswa, daftarMataKuliah);

        titleLabel = new JLabel();
        titleLabel.setText("Hapus IRS");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);
        
        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);

        // Panel untuk meletakkan titleLabel
        ttlPanel = new JPanel(new GridBagLayout());
        ttlPanel.setBackground(panelColor);
        ttlPanel.add(titleLabel);

        // Panel untuk meletakkan combobox
        cbPanel = new JPanel(new GridBagLayout());
        cbPanel.setBackground(panelColor);

        // Membuat label "Pilih NPM"
        label1 = new JLabel("Pilih NPM");
        label1.setFont(SistemAkademikGUI.fontGeneral);
        label1.setForeground(fontColor);
        gbc.gridx = 0;
        gbc.gridy = 0;
        cbPanel.add(label1, gbc);

        // Membuat combobox yang menampilakan npm yang sudah terurut
        box1 = new JComboBox<Long>(listNpm.toArray(new Long[0]));
        box1.setPreferredSize(new Dimension(125,23));
        gbc.gridy = 1;
        cbPanel.add(box1, gbc);

        // Membuat label "Pilih Nama Matkul"
        label2 = new JLabel("Pilih Nama Matkul");
        label2.setFont(SistemAkademikGUI.fontGeneral);
        label2.setForeground(fontColor);
        gbc.gridy = 2;
        cbPanel.add(label2, gbc);

        // Membuat combobox yang menampilakan nama matkul yang sudah terurut
        box2 = new JComboBox<String>(listMatkul.toArray(new String[0]));
        box2.setPreferredSize(new Dimension(100,23));
        gbc.gridy = 3;
        cbPanel.add(box2, gbc);

        // Panel untuk meletakkan button-button
        btnPanel = new JPanel(new GridBagLayout());
        btnPanel.setBackground(panelColor);

        // Membuat button "Hapus"
        button1 = new JButton("Hapus");
        button1.setFont(SistemAkademikGUI.fontGeneral);
        button1.setBackground(button1Color);
        button1.setForeground(fontColor);
        button1.setBorderPainted(false);
        button1.setFocusable(false);
        gbc.gridy = 0;
        btnPanel.add(button1, gbc);

        // Membuat button "Kembali"
        button2 = new JButton("Kembali");
        button2.setFont(SistemAkademikGUI.fontGeneral);
        button2.setBackground(button2Color);
        button2.setForeground(fontColor);
        button2.setBorderPainted(false);
        button2.setFocusable(false);
        gbc.gridy = 1;
        btnPanel.add(button2, gbc);

        gbc.gridy = 0;
        mainPanel.add(ttlPanel, gbc);
        
        gbc.gridy = 1;
        mainPanel.add(cbPanel,gbc);

        gbc.gridy = 2;
        mainPanel.add(btnPanel,gbc);
        
        frame.add(mainPanel);

        // Menambahkan action listener ke button "Hapus" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                
                // Jika masih ada field yang kosong, tampilkan pesan berikut
                if(box1.getSelectedItem() == null | box2.getSelectedItem() == null) {
                    String pesan = "Mohon isi seluruh field";
                    JOptionPane.showMessageDialog(null, pesan);
                
                // Jika kedua field sudah terisi, kode ini akan dijalankan
                } else {
                    // Mendapatkan data yang dipilih
                    long npm = (long) box1.getSelectedItem();
                    String matkul = (String) box2.getSelectedItem();

                    // Mendapatkan objek mahasiswa berdasarkan npm
                    Mahasiswa mahasiswa = getMahasiswa(daftarMahasiswa, npm);
                    
                    // Mendapatkan objek mata kuliah berdasarkan nama matkul
                    MataKuliah mataKuliah = getMataKuliah(daftarMataKuliah, matkul);
                    
                    // Hapus objek matkul dari objek mahasiswa menggunakan method dropMatkul
                    String pesan = mahasiswa.dropMatkul(mataKuliah);

                    // Tampilkan pesan
                    JOptionPane.showMessageDialog(null, pesan);
                }
            }
        });

        // Menambahkan action listener ke button "Kembali" (button2)
        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HomeGUI(frame,daftarMahasiswa,daftarMataKuliah);
                frame.repaint();
                frame.revalidate();
            }
        });
    }

    // Uncomment method di bawah jika diperlukan

    // Method untuk mendapatkan objek mata kuliah berdasarkan nama
    private MataKuliah getMataKuliah(ArrayList<MataKuliah> daftarMataKuliah, String matkul) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(matkul)){
                return mataKuliah;
            }
        }
        return null;
    }

    // Method untuk mendapatkan objek mahasiswa berdasarkan npm
    private Mahasiswa getMahasiswa(ArrayList<Mahasiswa> daftarMahasiswa, long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }

    // Method untuk membuat listNpm dan listMatkul
    private void createListNpmAndMatkul(ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        for(Mahasiswa mahasiswa : daftarMahasiswa) {
            listNpm.add(mahasiswa.getNpm());
        }
        for(MataKuliah mataKuliah : daftarMataKuliah) {
            listMatkul.add(mataKuliah.getNama());
        }
    }

    // Method untuk sort objek mahasiswa berdasarkan npm
    private void sortNpm(ArrayList<Mahasiswa> daftarMahasiswa) {
        for(int i = 0; i < daftarMahasiswa.size(); i++) {
            for(int j = 0; j < daftarMahasiswa.size() - 1; j++) {
                if(daftarMahasiswa.get(j).getNpm() > daftarMahasiswa.get(j+1).getNpm()) {
                    Mahasiswa tmp = daftarMahasiswa.get(j);
                    daftarMahasiswa.set(j, daftarMahasiswa.get(j+1));
                    daftarMahasiswa.set(j+1, tmp);
                }
            }
        }
    }

    // Method untuk sort objek matkul berdasarkan nama
    private void sortMatkul(ArrayList<MataKuliah> daftarMataKuliah) {
        for(int i = 0; i < daftarMataKuliah.size(); i++) {
            for(int j = 0; j < daftarMataKuliah.size() - 1; j++) {
                if(daftarMataKuliah.get(j).getNama().compareTo(daftarMataKuliah.get(j+1).getNama()) > 0) {
                    MataKuliah tmp = daftarMataKuliah.get(j);
                    daftarMataKuliah.set(j, daftarMataKuliah.get(j+1));
                    daftarMataKuliah.set(j+1, tmp);
                }
            }
        }
    }
}
