package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    JLabel titleLabel, labelDaftarMahasiswa, label1, label2, label3, label4, label5, label6;
    JButton button1;
    JPanel mainPanel;
    GridBagConstraints gbc = new GridBagConstraints();
    Color buttonColor = new Color(72,125,195);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);
    String nama, kode;
    int sks, jumlahMahasiswa, kapasitas;
    Mahasiswa[] mahasiswa;

    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        
        // Dapatkan atribut-atribut mata kuliah yang akan ditampilkan
        nama = mataKuliah.getNama();
        kode = mataKuliah.getKode();
        sks = mataKuliah.getSKS();
        jumlahMahasiswa = mataKuliah.getJumlahMahasiswa();
        kapasitas = mataKuliah.getKapasitas();
        mahasiswa = mataKuliah.getDaftarMahasiswa();

        titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);

        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);
        
        // Variabel untuk mengatur peletakan komponen secara vertikal, akan diincrement setiap komponen baru diletakkan
        int i = 0;

        gbc.gridx = 0;
        gbc.gridy = i++;
        mainPanel.add(titleLabel, gbc);

        // Membuat label yang menampilkan nama mata kuliah
        label1 = new JLabel(String.format("Nama mata kuliah: %s", nama));
        label1.setFont(SistemAkademikGUI.fontGeneral);
        label1.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label1, gbc);

        // Membuat label yang menampilkan kode matkul
        label2 = new JLabel(String.format("Kode: %s", kode));
        label2.setFont(SistemAkademikGUI.fontGeneral);
        label2.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label2, gbc);

        // Membuat label yang menampilkan sks matkul
        label3 = new JLabel(String.format("SKS: %d", sks));
        label3.setFont(SistemAkademikGUI.fontGeneral);
        label3.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label3, gbc);

        // Membuat label yang menampilkan jumlah mahasiswa yang mengambil matkul
        label4 = new JLabel(String.format("Jumlah mahasiswa: %d", jumlahMahasiswa));
        label4.setFont(SistemAkademikGUI.fontGeneral);
        label4.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label4, gbc);

        // Membuat label yang menampilkan kapasitas matkul
        label5 = new JLabel(String.format("Kapasitas: %d", kapasitas));
        label5.setFont(SistemAkademikGUI.fontGeneral);
        label5.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label5, gbc);

        // Membuat label "Daftar Mahasiswa:"
        label6 = new JLabel("Daftar Mahasiswa:");
        label6.setFont(SistemAkademikGUI.fontGeneral);
        label6.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label6, gbc);

        // Jika belum ada mahasiswa yang mengambil matkul, kode berikut akan dijalankan
        if(jumlahMahasiswa == 0) {
            
            // Membuat label yang menampilkan string berikut
            labelDaftarMahasiswa = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            labelDaftarMahasiswa.setFont(new Font("Century Gothic", Font.BOLD , 12));
            labelDaftarMahasiswa.setForeground(fontColor);
            gbc.gridy = i++;
            mainPanel.add(labelDaftarMahasiswa, gbc);
        
        // Jika sudah ada matkul yang diambil, kode berikut akan dijalankan
        } else {
            
            // Loop yang akan membuat label baru yang menampilkan nama mahasiswa yang mengambil matkul
            for(int j = 0; j < mahasiswa.length; j++) {
                if(mahasiswa[j] != null) {
                    labelDaftarMahasiswa = new JLabel(String.format("%d. %s", j+1, mahasiswa[j].getNama()));
                    labelDaftarMahasiswa.setFont(new Font("Century Gothic", Font.BOLD , 12));
                    labelDaftarMahasiswa.setForeground(fontColor);
                    gbc.gridy = i++;
                    mainPanel.add(labelDaftarMahasiswa, gbc);
                }
                
            }
        }

        // Membuat button "Selesai"
        button1 = new JButton("Selesai");
        button1.setFont(SistemAkademikGUI.fontGeneral);
        button1.setBackground(buttonColor);
        button1.setForeground(fontColor);
        button1.setBorderPainted(false);
        button1.setFocusable(false);
        gbc.gridy = i;
        mainPanel.add(button1, gbc);

        frame.add(mainPanel);

        // Menambahkan action listener ke button "Selesai" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HomeGUI(frame,daftarMahasiswa,daftarMataKuliah);
                frame.setSize(500,500);
                frame.setLocationRelativeTo(null);
                frame.repaint();
                frame.revalidate();
            }
        });
        
    }
}
