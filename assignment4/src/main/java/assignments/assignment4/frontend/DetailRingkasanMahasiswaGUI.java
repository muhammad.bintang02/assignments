package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    JLabel titleLabel, labelMatkul, labelMasalahIRS, label1, label2, label3, label4, label5, label6;
    JButton button1;
    JPanel mainPanel;
    GridBagConstraints gbc = new GridBagConstraints();
    Color buttonColor = new Color(72,125,195);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);
    String nama, jurusan;
    long npm;
    int totalSKS, banyakMatkul, banyakMasalahIRS; 
    MataKuliah[] mataKuliah;
    String[] masalahIRS;

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mahasiswa        

        // Lakukan pengecekan irs menggunakan method cekIRS
        mahasiswa.cekIRS();    
        
        // Dapatkan atribut-atribut mahasiswa yang akan ditampilkan
        nama = mahasiswa.getNama();
        jurusan = mahasiswa.getJurusan();
        npm = mahasiswa.getNpm();
        totalSKS = mahasiswa.getTotalSKS();
        banyakMatkul = mahasiswa.getBanyakMatkul();
        banyakMasalahIRS = mahasiswa.getBanyakMasalahIRS();
        masalahIRS = mahasiswa.getMasalahIRS();
        mataKuliah = mahasiswa.getMataKuliah();
        
        titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);

        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);
        
        // Variabel untuk mengatur peletakan komponen secara vertikal, akan diincrement setiap komponen baru diletakkan
        int i = 0;

        gbc.gridx = 0;
        gbc.gridy = i++;
        mainPanel.add(titleLabel, gbc);

        // Membuat label yang menampilkan nama mahasiswa
        label1 = new JLabel(String.format("Nama: %s", nama));
        label1.setFont(SistemAkademikGUI.fontGeneral);
        label1.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label1, gbc);

        // Membuat label yang menampilkan npm mahasiswa
        label2 = new JLabel(String.format("NPM: %d", npm));
        label2.setFont(SistemAkademikGUI.fontGeneral);
        label2.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label2, gbc);

        // Membuat label yang menampilkan jurusan mahasiswa
        label3 = new JLabel(String.format("Jurusan: %s", jurusan));
        label3.setFont(SistemAkademikGUI.fontGeneral);
        label3.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label3, gbc);

        // Membuat label "Daftar Mata Kuliah:"
        label4 = new JLabel("Daftar Mata Kuliah:");
        label4.setFont(SistemAkademikGUI.fontGeneral);
        label4.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label4, gbc);

        // Jika belum ada matkul yang belum diambil, kode berikut akan dijalankan
        if(banyakMatkul == 0) {

            // Membuat label yang menampilkan string berikut
            labelMatkul = new JLabel("Belum ada mata kuliah yang diambil.");
            labelMatkul.setFont(new Font("Century Gothic", Font.BOLD , 12));
            labelMatkul.setForeground(fontColor);
            gbc.gridy = i++;
            mainPanel.add(labelMatkul, gbc);
        
        // Jika sudah ada matkul yang diambil, kode berikut akan dijalankan
        } else {

            // Loop yang akan membuat label baru yang menampilkan nama mata kuliah yang diambil mahasiswa
            for(int j = 0; j < mataKuliah.length; j++) {
                if(mataKuliah[j] != null) {
                    labelMatkul = new JLabel(String.format("%d. %s", j+1, mataKuliah[j].getNama()));
                    labelMatkul.setFont(new Font("Century Gothic", Font.BOLD , 12));
                    labelMatkul.setForeground(fontColor);
                    gbc.gridy = i++;
                    mainPanel.add(labelMatkul, gbc);
                }
            }
        }

        // Membuat label yang menampilkan total sks mahasiswa
        label5 = new JLabel(String.format("Total SKS: %d", totalSKS));
        label5.setFont(SistemAkademikGUI.fontGeneral);
        label5.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label5, gbc);

        // Membuat label "Hasil Pengecekan IRS:"
        label6 = new JLabel("Hasil Pengecekan IRS:");
        label6.setFont(SistemAkademikGUI.fontGeneral);
        label6.setForeground(fontColor);
        gbc.gridy = i++;
        mainPanel.add(label6, gbc);

        // Jika tidak ada masalah irs, kode berikut akan dijalankan
        if(banyakMasalahIRS == 0) {

            // Membuat label yang menampilkan string berikut
            labelMasalahIRS = new JLabel("IRS tidak bermasalah.");
            labelMasalahIRS.setFont(new Font("Century Gothic", Font.BOLD , 12));
            labelMasalahIRS.setForeground(fontColor);
            gbc.gridy = i++;
            mainPanel.add(labelMasalahIRS, gbc);
        
        // Jika ada masalah irs, kode berikut akan dijalankan
        } else {

            // Loop yang akan membuat label baru yang menampilkan masalah irs
            for(int j = 0; j < masalahIRS.length; j++) {
                if(masalahIRS[j] != null) {
                    labelMasalahIRS = new JLabel(String.format("%d. %s", j+1, masalahIRS[j]));
                    labelMasalahIRS.setFont(new Font("Century Gothic", Font.BOLD , 12));
                    labelMasalahIRS.setForeground(fontColor);
                    gbc.gridy = i++;
                    mainPanel.add(labelMasalahIRS, gbc);
                }
            }
        }

        // Membuat button "Selesai"
        button1 = new JButton("Selesai");
        button1.setFont(SistemAkademikGUI.fontGeneral);
        button1.setBackground(buttonColor);
        button1.setForeground(fontColor);
        button1.setBorderPainted(false);
        button1.setFocusable(false);
        gbc.gridy = i;
        mainPanel.add(button1, gbc);

        frame.add(mainPanel);

        // Menambahkan action listener ke button "Selesai" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HomeGUI(frame,daftarMahasiswa,daftarMataKuliah);
                frame.setSize(500,500); // Set ukuran frame ke ukuran normal
                frame.setLocationRelativeTo(null);
                frame.repaint();
                frame.revalidate();
            }
        });

    }
}
