package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {
    ArrayList<String> listMatkul = new ArrayList<String>();
    JLabel titleLabel, label1;
    JButton button1, button2;
    JComboBox<String> box1;
    JPanel mainPanel, btnPanel, ttlPanel, cbPanel;
    GridBagConstraints gbc = new GridBagConstraints();
    Color button1Color = new Color(72,125,195);
    Color button2Color = new Color(156,198,52);
    Color panelColor = new Color(32,33,36);
    Color fontColor = new Color(249,246,238);

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mata Kuliah
        
        // Sort arraylist daftarMataKuliah agar objek terurut secara alfabet
        sortMatkul(daftarMataKuliah);
        
        // Membuat listMatkul yang isinya nama matkul yang sudah terurut
        createListMatkul(daftarMataKuliah);

        titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(fontColor);

        // Semua panel menggunakan GridBagLayout

        // Set jarak antar komponen
        gbc.insets = new Insets(5,5,5,5);
        
        // Inisialisasi mainPanel yang nantinya digunakan untuk menaruh subpanel lainnya
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(panelColor);

        // Panel untuk meletakkan titleLabel
        ttlPanel = new JPanel(new GridBagLayout());
        ttlPanel.setBackground(panelColor);
        ttlPanel.add(titleLabel);

        // Panel untuk meletakkan combobox
        cbPanel = new JPanel(new GridBagLayout());
        cbPanel.setBackground(panelColor);

        // Membuat label "Pilih Nama Matkul"
        label1 = new JLabel("Pilih Nama Matkul");
        label1.setFont(SistemAkademikGUI.fontGeneral);
        label1.setForeground(fontColor);
        gbc.gridx = 0;
        gbc.gridy = 0;
        cbPanel.add(label1, gbc);

        // Membuat combobox yang menampilakan nama matkul yang sudah terurut
        box1 = new JComboBox<String>(listMatkul.toArray(new String[0]));
        box1.setPreferredSize(new Dimension(100,23));
        gbc.gridy = 1;
        cbPanel.add(box1, gbc);

        // Panel untuk meletakkan button-button
        btnPanel = new JPanel(new GridBagLayout());
        btnPanel.setBackground(panelColor);

        // Membuat button "Lihat"
        button1 = new JButton("Lihat");
        button1.setFont(SistemAkademikGUI.fontGeneral);
        button1.setBackground(button1Color);
        button1.setForeground(fontColor);
        button1.setBorderPainted(false);
        button1.setFocusable(false);
        gbc.gridy = 0;
        btnPanel.add(button1, gbc);

        // Membuat button "Kembali"
        button2 = new JButton("Kembali");
        button2.setFont(SistemAkademikGUI.fontGeneral);
        button2.setBackground(button2Color);
        button2.setForeground(fontColor);
        button2.setBorderPainted(false);
        button2.setFocusable(false);
        gbc.gridy = 1;
        btnPanel.add(button2, gbc);

        gbc.gridy = 0;
        mainPanel.add(ttlPanel, gbc);
        
        gbc.gridy = 1;
        mainPanel.add(cbPanel,gbc);

        gbc.gridy = 2;
        mainPanel.add(btnPanel,gbc);
        
        frame.add(mainPanel);

        // Menambahkan action listener ke button "Lihat" (button1)
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                
                // Jika masih ada field yang kosong, tampilkan pesan berikut
                if(box1.getSelectedItem() == null) {
                    String pesan = "Mohon isi seluruh field";
                    JOptionPane.showMessageDialog(null, pesan);
                
                // Jika field sudah terisi, kode ini akan dijalankan
                } else {
                    
                    // Mendapatkan data yang dipilih
                    String matkul = (String) box1.getSelectedItem();
                   
                    // Mendapatkan objek mata kuliah berdasarkan nama
                    MataKuliah mataKuliah = getMataKuliah(daftarMataKuliah, matkul);

                    frame.getContentPane().removeAll();
                    new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah); // Inisialisasi objek DetailRingkasanMataKuliahGUI
                    frame.setSize(500,700); 
                    frame.setLocationRelativeTo(null);
                    frame.repaint();
                    frame.revalidate();
                }
            }
        });

        // Menambahkan action listener ke button "Kembali" (button2)
        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.getContentPane().removeAll();
                new HomeGUI(frame,daftarMahasiswa,daftarMataKuliah);
                frame.repaint();
                frame.revalidate();
            }
        });
        
    }

    private MataKuliah getMataKuliah(ArrayList<MataKuliah> daftarMataKuliah, String matkul) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(matkul)){
                return mataKuliah;
            }
        }
        return null;
    }

    private void createListMatkul(ArrayList<MataKuliah> daftarMataKuliah) {
        for(MataKuliah mataKuliah : daftarMataKuliah) {
            listMatkul.add(mataKuliah.getNama());
        }
    }

    private void sortMatkul(ArrayList<MataKuliah> daftarMataKuliah) {
        for(int i = 0; i < daftarMataKuliah.size(); i++) {
            for(int j = 0; j < daftarMataKuliah.size() - 1; j++) {
                if(daftarMataKuliah.get(j).getNama().compareTo(daftarMataKuliah.get(j+1).getNama()) > 0) {
                    MataKuliah tmp = daftarMataKuliah.get(j);
                    daftarMataKuliah.set(j, daftarMataKuliah.get(j+1));
                    daftarMataKuliah.set(j+1, tmp);
                }
            }
        }
    }
}
