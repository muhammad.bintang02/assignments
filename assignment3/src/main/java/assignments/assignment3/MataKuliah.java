package assignments.assignment3;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private int counterKapasitas;   // Atribut untuk menghitung kapasitas kelas yang tersisa

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];    // Inisiasi array daftarMahasiswa dengan panjang kapasitas
        this.counterKapasitas = kapasitas;  // Nilai awal counterKapasitas sama dengan kapasitas
    }

    // Getter
    public Dosen getDosen() {
        /* TODO: implementasikan kode Anda di sini */
        return this.dosen;
    }

    public int getCounterKapasitas() {
        return this.counterKapasitas;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    // Method untuk menambahkan mahasiswa ke array daftarMahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan menambahkan objek mahasiswa pada parameter ke array daftarMahasiswa
        for (int i = 0; i < this.daftarMahasiswa.length; i++) {
            if (this.daftarMahasiswa[i] == null) {
                this.daftarMahasiswa[i] = mahasiswa;    
                this.counterKapasitas -= 1; // counterKapasitas berkurang 1 setelah mahasiswa ditambahkan
                break;  
            }
        }
    }

    // Method untuk menghapus mahasiswa dari array daftarMahasiswa
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan menghapus objek mahasiswa pada parameter dari array daftarMahasiswa
        for (int i = 0; i < this.daftarMahasiswa.length; i++) {

            // Memastikan objek yang sedang diiterasi sama dengan objek mahasiswa yang ingin dihapus
            if (this.daftarMahasiswa[i] == mahasiswa) { 

                // Loop yang akan menggeser elemen pada array ke kiri, dimulai dari posisi objek mahasiswa yang ingin dihapus sehingga objek tersebut tertimpa dengan objek di sebelahnya
                for (int j = i; j < this.daftarMahasiswa.length - 1; j++) {
                    this.daftarMahasiswa[j] = this.daftarMahasiswa[j+1];
                }

                // Mengganti elemen terakhir dengan null setelah proses pergesearn selesai
                this.daftarMahasiswa[this.daftarMahasiswa.length-1] = null;

                // counterKapasitas bertambah 1 setelah objek mahasiswa dihapus
                this.counterKapasitas += 1; 
            }
        }
    }

    // Method untuk mengecek apakah array daftarMahasiswa isinya null semua atau tidak
    public boolean cekDaftarMahasiswa() {
        for (int i = 0; i < this.daftarMahasiswa.length; i++) {
            if(this.daftarMahasiswa[i] != null) {
                return true;    // Jika ada elemen yang tidak null, akan direturn true
            }
        }
        return false;   // Jika semua elemennya adalah null, maka akan direturn false
    }

    // Method untuk mencetak ringkasan mata kuliah
    public void ringkasanMataKuliah() {
        System.out.println("Nama mata kuliah: " + this.nama);
        System.out.println("Jumlah mahasiswa: " + (this.kapasitas - this.counterKapasitas));
        System.out.println("Kapasitas: " + this.kapasitas);
        System.out.print("Dosen pengajar: ");

        // Jika atribut dosen bernilai null, akan dicetak string berikut
        if(this.dosen == null) {
            System.out.println("Belum ada");
        } else {
            System.out.println(this.dosen.toString());
        }

        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");

        // Jika array daftarMahasiswa isinya null semua, akan dicetak string berikut
        if(!cekDaftarMahasiswa()) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");

        // Jika sudah ada mahasiswa yang mengambil mata kuliah ini, maka kode ini akan dijalankan
        } else {

            // Loop yang mencetak nama mahasiswa pada array daftarMahasiswa
            for (int i = 0; i < this.daftarMahasiswa.length; i++) {    
                if (this.daftarMahasiswa[i] != null) {
                    System.out.println(i+1 + ". " + this.daftarMahasiswa[i].toString());
                }
            }
        }

    }

    // Method untuk menambahkan objek dosen ke objek mata kuliah
    public void addDosen(Dosen dosen) {
        /* TODO: implementasikan kode Anda di sini */

        // Set atribut dosen dengan parameter yang diberikan
        this.dosen = dosen;
    }

    // Method untuk menghapus objek dosen dari objek mata kuliah
    public void dropDosen() {
        /* TODO: implementasikan kode Anda di sini */

        // Set atribut dosen menjadi null
        this.dosen = null;
    }
}