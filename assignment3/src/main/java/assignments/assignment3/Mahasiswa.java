package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        super("Mahasiswa", nama);   // Set atribut menggunakan super
        this.npm = npm; // Set npm
    }

    // Method untuk mengecek apakah mahasiswa sudah mengambil mata kuliah pada parameter atau belum
    public boolean cekMatkul(MataKuliah mataKuliah) {   
        for(int i = 0; i < this.daftarMataKuliah.length; i++) {
            if (this.daftarMataKuliah[i] == mataKuliah) {
                return true;   // Jika objek mata kuliah sudah diambil mahasiswa, akan direturn true
            } 
        } 
        return false;    // Jika belum ada, akan direturn false
    }

    // Method untuk mengecek apakah mata kuliah yang ingin ditambahkan kapasitasnya sudah penuh atau belum
    public boolean cekKapasitas(MataKuliah mataKuliah) {
        if (mataKuliah.getCounterKapasitas() > 0) {
            return true;    // Jika masih ada tempat kosong, maka akan direturn true
        } else {
            return false;   // Jika sudah penuh, akan direturn false
        }
    }

    // Method untuk menambahkan objek mataKuliah ke array daftarMataKuliah dan objek mahasiswa ke array daftarMahasiswa
    public void addMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */

        // Memastikan mata kuliah yang ditambahkan belum pernah diambil dan masih tersedia kapasitasnya
        if(!cekMatkul(mataKuliah) & cekKapasitas(mataKuliah)) {

            // Loop yang akan menambahkan objek mataKuliah ke array daftarMataKuliah
            for (int i = 0; i < this.daftarMataKuliah.length; i++) {
                if (this.daftarMataKuliah[i] == null) {
                    this.daftarMataKuliah[i] = mataKuliah;
                    break;
                }
            }

            // Menambahkan objek mahasiswa ke array daftarMahasiswa menggunakan method addMahasiswa()
            mataKuliah.addMahasiswa(this);

            // Cetak string sesuai ketentuan
            System.out.println(String.format("%s berhasil menambahkan mata kuliah %s", super.toString(), mataKuliah.toString()));


        // Jika mata kuliah sudah diambil, cetak string berikut
        } else if (cekMatkul(mataKuliah)) {
            System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya", mataKuliah.toString()));
        
        // Jika mata kuliah sudah penuh kapasitasnya, cetak string berikut
        } else {
            System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya", mataKuliah.toString()));
        }
        
    }

    // Method untuk menghapus mataKuliah dari array daftarMataKuliah
    public void dropMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        
        // Jika mata kuliah yang ingin dihapus belum pernah diambil, cetak string berikut
        if(!cekMatkul(mataKuliah)) {
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil", mataKuliah.toString()));
        
        // Jika sudah pernah diambil, maka kode ini akan dijalankan
        } else {

            // Loop yang akan menghapus objek mataKuliah pada parameter dari array daftarMataKuliah
            for (int i = 0; i < this.daftarMataKuliah.length; i++) {

                // Memastikan objek yang sedang diiterasi sama dengan objek mata kuliah yang ingin dihapus
                if (this.daftarMataKuliah[i] == mataKuliah) { 
    
                    // Loop yang akan menggeser objek mata kuliah pada array ke kiri, dimulai dari posisi objek mahasiswa yang ingin dihapus sehingga objek tersebut tertimpa dengan objek di sebelahnya
                    for (int j = i; j < this.daftarMataKuliah.length - 1; j++) {
                        this.daftarMataKuliah[j] = this.daftarMataKuliah[j+1];
                    }

                    // Mengganti elemen terakhir dengan null setelah proses pergesearn selesai
                    this.daftarMataKuliah[this.daftarMataKuliah.length-1] = null;
                }
            }

            // Menghapus objek mahasiswa dari array daftarMahasiswa menggunakan method dropMahasiswa()
            mataKuliah.dropMahasiswa(this);

            // Cetak string sesuai ketentuan
            System.out.println(String.format("%s berhasil drop mata kuliah %s", super.toString(), mataKuliah.toString()));
        }
    }

    // Method untuk extract tanggal lahir dari npm
    public String extractTanggalLahir(long npm) {
        /* TODO: implementasikan kode Anda di sini */

        // Mengubah npm menjadi string
        String stringNPM = String.valueOf(npm);

        // Mendapatkan tanggal lahir menggunakan method substring

        // Jika tanggal dan bulan diawali 0, maka angka 0 tersebut tidak ikut dimasukkan
        if(Integer.parseInt(stringNPM.substring(4,5)) == 0 & Integer.parseInt(stringNPM.substring(6,7)) == 0) { 
            this.tanggalLahir = stringNPM.substring(5,6) + "-" + stringNPM.substring(7,8) + "-" + stringNPM.substring(8,12);
            return this.tanggalLahir;

        // Jika tanggal diawali 0, maka angka 0 tersebut tidak ikut dimasukkan
        } else if(Integer.parseInt(stringNPM.substring(4,5)) == 0 & Integer.parseInt(stringNPM.substring(6,7)) != 0) {  
            this.tanggalLahir = stringNPM.substring(5,6) + "-" + stringNPM.substring(6,8) + "-" + stringNPM.substring(8,12);
            return this.tanggalLahir;

        // Jika bulan diawali 0, maka angka 0 tersebut tidak ikut dimasukkan
        } else if(Integer.parseInt(stringNPM.substring(4,5)) != 0 & Integer.parseInt(stringNPM.substring(6,7)) == 0) {  
            this.tanggalLahir = stringNPM.substring(4,6) + "-" + stringNPM.substring(7,8) + "-" + stringNPM.substring(8,12);
            return this.tanggalLahir;

        // Jika tanggal dan bulan tidak diawali 0, maka kode ini dijalankan
        } else {
            this.tanggalLahir = stringNPM.substring(4,6) + "-" + stringNPM.substring(6,8) + "-" + stringNPM.substring(8,12);
            return this.tanggalLahir;
        }
    }

    // Method untuk extract jurusan dari npm
    public String extractJurusan(long npm) {
        /* TODO: implementasikan kode Anda di sini */

        // Mengubah npm menjadi string
        String stringNPM = String.valueOf(npm);

        if (stringNPM.substring(2,4).equals("01")) {    // Mendapatkan kode jurusan dengan method substring
            this.jurusan = "Ilmu Komputer";
            return this.jurusan;    // Jika kode jurusan "01", maka jurusannya adalah Ilmu Komputer
        } else {
            this.jurusan = "Sistem Informasi";
            return this.jurusan;    // Jika kode jurusan "02", maka jurusannya adalah Sistem Informasi
        }
    }

    // Method untuk mengecek apakah elemen pada array daftarMataKuliah null semua atau tidak
    public boolean cekDaftarMataKuliah() {
        for (int i = 0; i < this.daftarMataKuliah.length; i++) {
            if(this.daftarMataKuliah[i] != null) {
                return true;    // Jika ada elemen yang tidak null, akan direturn true
            }
        }
        return false;   // Jika semua elemennya adalah null, maka akan direturn false
    }

    // Method untuk mencetak ringkasan mahasiswa
    public void ringkasanMahasiswa() {
        String nama = super.toString();     
        String tanggalLahir = extractTanggalLahir(this.npm);    
        String jurusan = extractJurusan(this.npm);

        System.out.println("Nama: " + nama);
        System.out.println("Tanggal lahir: " + tanggalLahir);
        System.out.println("Jurusan: " + jurusan);
        System.out.println("Daftar Mata Kuliah:");

        // Jika array daftarMataKuliah isinya null semua, cetak string berikut
        if(!cekDaftarMataKuliah()) {
            System.out.println("Belum ada mata kuliah yang diambil");

        // Jika sudah ada matkul yang diambil, maka block kode ini akan dijalankan
        } else {

            // Loop yang mencetak nama mata kuliah dari objek pada array mataKuliah
            for (int i = 0; i < this.daftarMataKuliah.length; i++) {    
                if (this.daftarMataKuliah[i] != null) {
                    System.out.println(i+1 + ". " + this.daftarMataKuliah[i].toString());
                }
            }
        }
    }

    // Method untuk mengecek apakah mahasiswa mengambil mata kuliah yang diajar oleh dosen pada parameter
    public boolean cekDosen(String dosen) {

        // Loop yang akan mengecek apakah terdapat mata kuliah mahasiswa yang dosen pengajarnya sama dengan dosen pada parameter
        for(int i = 0; i < this.daftarMataKuliah.length; i++) {
            if(this.daftarMataKuliah[i] != null) {

                // Mendapatkan objek dosen menggunakan method getDosen
                Dosen objekDosen = this.daftarMataKuliah[i].getDosen();
                
                // Memastikan dosen tidak null
                if(objekDosen != null) {

                    // Jika objek dosen memiliki nama yang sama dengan dosen pada parameter, return true
                    if(objekDosen.toString().equals(dosen)) {
                        return true;
                    }
                }
                
            }
        }

        // Jika tidak ada dosen yang sama dengan dosen pada parameter, return false
        return false;
    }
}