package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("Dosen", nama);   // Set atribut menggunakan super
    }

    // Method untuk menambahkan mata kuliah yang diajar dosen
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */   

        // Jika dosen belum mengajar mata kuliah apapaun dan mata kuliah yang ingin diajar belum memiliki dosen, maka block kode ini akan dijalankan
        if((this.mataKuliah == null) & (mataKuliah.getDosen() == null)) {
            
            // Set atribut mataKuliah dengan parameter yang diberikan
            this.mataKuliah = mataKuliah;

            // Menambahkan objek dosen ke objek mataKuliah yang diajar menggunakan method addDosen()
            mataKuliah.addDosen(this);

            // Cetak string sesuai ketentuan
            System.out.println(String.format("%s mengajar mata kuliah %s", super.toString(), mataKuliah.toString()));
       
        // Jika dosen sudah mengajar suatu mata kuliah, cetak string berikut
        } else if(this.mataKuliah != null) {
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", super.toString(), this.mataKuliah.toString()));
       
        // Jika mata kuliah yang diajar sudah memiliki dosen pengajar, cetak string berikut
        } else {
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar", mataKuliah.toString()));
        }

    }

    // Method untuk menghapus mata kuliah yang diajar dosen
    public void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        
        // Jika dosen mengajar suatu mata kuliah, maka kode ini akan dijalankan
        if(this.mataKuliah != null) {

            // Mendapatkan nama mata kuliah yang akan didrop
            String nama = this.mataKuliah.toString();

            // Drop objek dosen dari objek mataKuliah menggunakan method dropDosen()
            this.mataKuliah.dropDosen();

            // Set atribut mataKuliah milik objek dosen menjadi null
            this.mataKuliah = null;

            // Cetak string sesuai ketentuan
            System.out.println(String.format("%s berhenti mengajar %s", super.toString(), nama));

        // Jika dosen belum mengajar mata kuliah, cetak string berikut
        } else {
            System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun", super.toString()));
        }
    }
}