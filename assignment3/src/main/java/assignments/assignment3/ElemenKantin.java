package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */
    
    private Makanan[] daftarMakanan = new Makanan[10];

    public ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("ElemenKantin", nama);    // Set atribut menggunakan super
    }

    // Getter
    public Makanan[] getDaftarMakanan() {
        return this.daftarMakanan;
    }

    // Method untuk mengecek apakah makanan dengan nama sesuai parameter sudah ada di array daftarMakanan atau belum
    public boolean cekMakanan(String nama) {

        // Loop yang akan mengecek apakah terdapat elemen di array daftarMakanan yang memiliki nama sama dengan parameter
        for(int i = 0; i < this.daftarMakanan.length; i++) {
            if(this.daftarMakanan[i] != null) {
                if(this.daftarMakanan[i].toString().equals(nama)) {
                    return true;    // Return true jika terdapat elemen yang memiliki nama yang sama dengan paramater
                }
            } 
        }
        return false;   // Return false jika tidak ada elemen yang memiliki nama yang sama dengan parameter
    }

    // Method yang akan menambahkan makanan ke array daftarMakanan
    public void setMakanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        
        // Jika makanan yang ingin ditambahkan belum ada di array daftarMakanan, maka kode ini akan dijalankan
        if(!cekMakanan(nama)) {

            // Loop yang akan membuat objek makanan baru dengan atribut sesuai dengan parameter dan menyimpannya di array daftarMakanan
            for(int i = 0; i < this.daftarMakanan.length; i++) {
                if(this.daftarMakanan[i] == null) {
                    this.daftarMakanan[i] = new Makanan(nama, harga);
                    break;
                }
            }

            // Cetak string sesuai ketentuan
            System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %d", super.toString(), nama, harga));
        
        // Jika makanan yang ingin ditambahkan sudah ada di dalam array daftarMakanan, cetak string berikut
        } else {
            System.out.println(String.format("[DITOLAK] %s sudah pernah terdaftar", nama));
        }

    }
}