package assignments.assignment3;

import java.util.Scanner;
import java.util.Arrays;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0; 

    // Method untuk menambahkan mahasiswa ke array daftarElemenFasilkom
    private static void addMahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */

        // Membuat objek mahasiswa baru dengan atribut sesuai dengan parameter
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);

        // Increment totalElemenFasilkom
        totalElemenFasilkom++; 

        // Cetak string sesuai ketentuan
        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    // Method untuk menambahkan dosen ke array daftarElemenFasilkom
    private static void addDosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = new Dosen(nama);
        totalElemenFasilkom++; 

        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    // Method untuk menambahkan ElemenKantin ke array daftarElemenFasilkom
    private static void addElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        totalElemenFasilkom++; 

        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    // Method untuk mendapatkan objek ElemenFasilkom berdasarkan nama
    private static ElemenFasilkom getElemenFasilkom (String nama) {
        
        // Loop yang akan mengecek apakah terdapat elemen pada array daftarElemenFasilkom yang memiliki nama yang sama dengan parameter
        for(int i = 0; i < daftarElemenFasilkom.length; i++) {
            if(daftarElemenFasilkom[i] != null) {
                if(daftarElemenFasilkom[i].toString().equals(nama)) {
                    return daftarElemenFasilkom[i]; // Return elemen tersebut jika namanya sama dengan parameter
                }
            }  
        }
        return null;    // Return null jika tidak ada yang sama dengan parameter
    }

    // Method untuk mendapatkan objek MataKuliah berdasarkan nama
    private static MataKuliah getMataKuliah (String nama) {
        for(int i = 0; i < daftarMataKuliah.length; i++) {
            if(daftarMataKuliah[i] != null) {
                if(daftarMataKuliah[i].toString().equals(nama)) {
                    return daftarMataKuliah[i];
                }
            }
        }
        return null;
    }

    private static void menyapa(String objek1, String objek2) {
        /* TODO: implementasikan kode Anda di sini */
        
        // Jika kedua objek memiliki nama yang sama, cetak string berikut
        if(objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        
        // Jika tidak, kode ini akan dijalankan
        } else {
            
            // Mendapatkan objek ElemenFasilkom menggunakan method getElemenFasilkom
            ElemenFasilkom elemenFasilkom1 = getElemenFasilkom(objek1);
            ElemenFasilkom elemenFasilkom2 = getElemenFasilkom(objek2);

            elemenFasilkom1.menyapa(elemenFasilkom2);

            // Jika objek yang saling menyapa adalah mahasiswa dan dosen, maka kode ini akan dijalankan
            if((elemenFasilkom1.getTipe().equals("Mahasiswa") & elemenFasilkom2.getTipe().equals("Dosen"))) {
                Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom1;
                
                // Jika mahasiswa dan dosen tersebut terhubung dalam mata kuliah yang sama, nilai friendship kedua objek tersebut bertambah 2
                if(mahasiswa.cekDosen(elemenFasilkom2.toString())) {
                    elemenFasilkom1.addFriendship(2);
                    elemenFasilkom2.addFriendship(2);
                }
            } else if((elemenFasilkom2.getTipe().equals("Mahasiswa") & elemenFasilkom1.getTipe().equals("Dosen"))) {
                Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom2;
                if(mahasiswa.cekDosen(elemenFasilkom1.toString())) {
                    elemenFasilkom1.addFriendship(2);
                    elemenFasilkom2.addFriendship(2);
                }
            }
        }
    }

    private static void addMakanan(String objek, String namaMakanan, long harga) {
        /* TODO: implementasikan kode Anda di sini */

        // Mendapatkan objek ElemenFasilkom
        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);

        // Jika objek yang melakukan addMakanan bukan ElemenKantin, cetak string berikut
        if(!elemenFasilkom.getTipe().equals("ElemenKantin")) {
            System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin", elemenFasilkom.toString()));
        
        // Jika iya, kode ini akan dijalankan
        } else {

            // Downcast objek menjadi ElemenKantin
            ElemenKantin elemenKantin = (ElemenKantin) elemenFasilkom;
            
            // Menggunakan method setMakanan untuk menambahkan makanan ke array daftarMakanan
            elemenKantin.setMakanan(namaMakanan, harga);
        }
    }

    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        
        // Mendapatkan objek ElemenFasilkom
        ElemenFasilkom pembeli = getElemenFasilkom(objek1);
        ElemenFasilkom penjual = getElemenFasilkom(objek2);

        // Jika penjual bukan ElemenKantin, cetak string berikut
        if(!penjual.getTipe().equals("ElemenKantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");

        // Jika penjual dan pembeli merupakan orang yang sama, cetak string berikut
        } else if (penjual.toString().equals(pembeli.toString())) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        
        // Jika memenuhi ketentuan, kode ini akan dijalankan
        } else {

            // Pembeli membeli makanan menggunakan method membeliMakanan()
            pembeli.membeliMakanan(pembeli, penjual, namaMakanan);
        }
    }

    // Method untuk menambahkan mata kuliah ke array daftarMataKuliah
    private static void createMatkul(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */

        // Membuat objek mata kuliah baru dengan atribut sesuai dengan parameter
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        
        // Increment totalMataKuliah
        totalMataKuliah++;
        
        // Cetak string sesuai ketentuan
        System.out.println(String.format("%s berhasil ditambahkan dengan kapasitas %d", nama, kapasitas));
    }

    private static void addMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);

        // Jika objek yang melakukan addMatkul bukan mahasiswa, cetak string berikut
        if(!elemenFasilkom.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        
        // Jika mahasiswa, kode ini akan dijalankan
        } else {
            // Downcast objek menjadi Mahasiswa
            Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;

            // Mendapatkan objek mata kuliah berdasarkan parameter
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

            // Menambahkan mata kuliah menggunakan method addMatkul()
            mahasiswa.addMatkul(mataKuliah);
        }
    }

    private static void dropMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);

        // Jika objek yang melakukan dropMatkul bukan mahasiswa, cetak string berikut
        if(!elemenFasilkom.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        } else {
            Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

            // Menghapus mata kuliah menggunakan method addMatkul()
            mahasiswa.dropMatkul(mataKuliah);
        }
    }

    private static void mengajarMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);

        // Jika objek yang melakukan mengajarMatkul bukan dosen, cetak string berikut
        if(!elemenFasilkom.getTipe().equals("Dosen")) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        } else {
            Dosen dosen = (Dosen) elemenFasilkom;
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

            // Menambahkan mata kuliah menggunakan method mengajarMataKuliah()
            dosen.mengajarMataKuliah(mataKuliah);
        }
    }

    private static void berhentiMengajar(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);

        // Jika objek yang melakukan berhentiMengajar bukan dosen, cetak string berikut
        if(!elemenFasilkom.getTipe().equals("Dosen")) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        } else {
            Dosen dosen = (Dosen) elemenFasilkom;

            // Menambahkan mata kuliah menggunakan method dropMataKuliah()
            dosen.dropMataKuliah();
        }
        
    }

    private static void ringkasanMahasiswa(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);

        // Jika objek yang melakukan ringkasanMahasiswa bukan mahasiswa, cetak string berikut
        if(!elemenFasilkom.getTipe().equals("Mahasiswa")) {
            System.out.println(String.format("[DITOLAK] %s bukan merupakan seorang mahasiswa", elemenFasilkom.toString()));
        } else {
            Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;

            // Menggunakan method ringkasanMahasiwa() untuk mendapatkan ringkasan mahasiswa
            mahasiswa.ringkasanMahasiswa();
        }
    }

    private static void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

        // Menggunakan method ringkasanMataKuliah() untuk mendapatkan ringkasan mata kuliah
        mataKuliah.ringkasanMataKuliah();
    }

    private static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan melakukan penambahan / pengurangan nilai friendship sesuai jumlah elemen yang disapa
        for(int i = 0; i < totalElemenFasilkom; i++) {

            int setengahElemenFasilkom = (totalElemenFasilkom - 1) / 2;

            if(daftarElemenFasilkom[i] != null) {
                if(daftarElemenFasilkom[i].getJumlahDisapa() >= setengahElemenFasilkom) {
                    daftarElemenFasilkom[i].addFriendship(10);
                } else {
                    daftarElemenFasilkom[i].addFriendship(-5);
                }
            }
            
            // Reset array telahMenyapa
            daftarElemenFasilkom[i].resetMenyapa();
        }

        // Cetak string ketentuan
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        
        // Cetak peringkat friendship
        friendshipRanking();
    }

    private static void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */

        // Membuat array baru dengan panjang totalElemenFasilkom yang akan menampung ElemenFasilkom (tanpa null)
        ElemenFasilkom[] friendshipRank = new ElemenFasilkom[totalElemenFasilkom];

        int j = 0;

        // Loop yang akan mengcopy elemen tidak null pada array daftarElemenFasilkom ke array friendshipRank
        for(int i = 0; i < daftarElemenFasilkom.length; i++) {
            if(daftarElemenFasilkom[i] != null) {
                friendshipRank[j] = daftarElemenFasilkom[i];
                j++;
            }
        }
        
        // Sort array friendshipRank
        Arrays.sort(friendshipRank);

        // Cetak nama dan nilai friendship setelah disort
        for(int i = 0; i < friendshipRank.length; i++) {
            System.out.println(i+1 + ". " + friendshipRank[i].toString() + "(" + friendshipRank[i].getFriendship() + ")");
        }
        
    }

    private static void programEnd() {
        /* TODO: implementasikan kode Anda di sini */
        
        // Cetak string sesuai ketentuan
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        
        // Cetak peringkat friendship
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}