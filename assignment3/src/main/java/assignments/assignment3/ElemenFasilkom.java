package assignments.assignment3;

abstract class ElemenFasilkom implements Comparable <ElemenFasilkom> { // Implements Comparable agar bisa override compareTo yang digunakan untuk sort
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String tipe;
    
    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    public ElemenFasilkom(String tipe, String nama) {
        this.tipe = tipe;
        this.nama = nama;
    }

    // Getter
    public String getTipe() {
        return this.tipe;
    }

    public ElemenFasilkom[] getTelahMenyapa() {
        return this.telahMenyapa;
    }

    public int getFriendship() {
        return this.friendship;
    }

    public String getNama() {
        return this.nama;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    // Method untuk menambahkan friendship sebanyak point pada parameter
    public void addFriendship(int point) {

        // Jika friendship bernilai lebih dari 100 setelah ditambahkan, maka set friendship menjadi 100 (maksimal)
        if(this.friendship + point > 100) {
            this.friendship = 100;

        // Jika friendship bernilai kurang dari 0 setelah ditambahkan, maka set friendship menjadi 0 (minimal)
        } else if(this.friendship + point < 0) {
            this.friendship = 0;
        
        } else {
            this.friendship += point;
        }
    }

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        /* TODO: implementasikan kode Anda di sini */
        
        // Jika sudah elemen pernah disapa, cetak string berikut
        if(this.cekTelahMenyapa(elemenFasilkom)) {
            System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini", this.toString(), elemenFasilkom.toString()));
        
        } else {
            // Loop yang akan memasukkkan objek pada parameter ke array telahMenyapa milik objek yang menyapa
            for(int i = 0; i < this.telahMenyapa.length; i++) {
                if(this.telahMenyapa[i] == null) {
                    this.telahMenyapa[i] = elemenFasilkom;
                    break;
                }
            }
            
            // Loop yang akan memasukkkan objek yang menyapa ke array telahMenyapa milik objek pada parameter
            for(int i = 0; i < elemenFasilkom.getTelahMenyapa().length; i++) {
                if(elemenFasilkom.getTelahMenyapa()[i] == null) {
                    elemenFasilkom.getTelahMenyapa()[i] = this;
                    break;
                }
            }
            
            // Cetak string sesuai ketentuan
            System.out.println(String.format("%s menyapa dengan %s", this.toString(), elemenFasilkom.toString()));
        }

    }

    public void resetMenyapa() {
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan mengganti elemen tidak null pada array telahMenyapa menjadi null
        for(int i = 0; i < this.telahMenyapa.length; i++) {
            if(this.telahMenyapa[i] != null) {
                this.telahMenyapa[i] = null;
            }
        }
    }

    // Method untuk mendapatkan objek makanan berdasarkan nama makanan dan objek penjual
    public Makanan getObjekMakanan(ElemenFasilkom penjual, String namaMakanan) {
        
        // Downcast penjual menjadi ElemenKantin
        ElemenKantin downCastedPenjual = (ElemenKantin) penjual;

        // Mendapatkan array daftarMakanan menggunakan getter milik ElemenKantin
        Makanan[] daftarMakanan = downCastedPenjual.getDaftarMakanan();
        
        // Loop yang akan mengecek apakah objek makanan yang ingin didapatkan terdapat pada array daftarMakanan atau tidak
        for(int i = 0; i < daftarMakanan.length; i++) {
            if(daftarMakanan[i] != null) {
                if(daftarMakanan[i].toString().equals(namaMakanan)) {
                    return daftarMakanan[i];    // Return objek makanan jika objek tersebut memiliki nama yang sama dengan paramater
                }
            }  
        }
        return null;    // Return null jika objek makanan tidak ditemukan
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */

        // Mendapatkan objek makanan menggunakan method getObjekMakanan()
        Makanan makanan = getObjekMakanan(penjual, namaMakanan);
        
        // Jika makanan tersebut dijual oleh penjual, maka block kode ini akan dijalankan
        if(makanan != null) {
            System.out.println(String.format("%s berhasil membeli %s seharga %d", pembeli, namaMakanan, makanan.getHarga()));

            // Menambahkan friendship pembeli dan penjual sebanyak 1
            pembeli.addFriendship(1);
            penjual.addFriendship(1);

        // Jika makanan tidak dijual oleh penjual, maka block kode ini akan dijalankan
        } else {
            System.out.println(String.format("[DITOLAK] %s tidak menjual %s", penjual, namaMakanan));
        }
    }

    // Method untuk mengecek apakah objek parameter sudah pernah disapa atau belum
    public boolean cekTelahMenyapa(ElemenFasilkom elemenFasilkom) {

        // Loop yang akan mengecek apakan objek pada parameter terdapat pada array telahMenyapa
        for(int i = 0; i < this.telahMenyapa.length; i++) {
            if(this.telahMenyapa[i] == elemenFasilkom) {
                return true;    // Return true jika objek sudah pernah disapa
            }
        }
        return false;   // Return false jika objek belum pernah disapa
    }

    // Method untuk menghitung jumlah elemen yang disapa
    public int getJumlahDisapa() {
        int count = 0;

        // Loop yang menghitung elemen bukan null pada array telahMenyapa
        for(int i = 0; i < this.telahMenyapa.length; i++) {
            if(this.telahMenyapa[i] != null) {
                count++;
            }
        }
        return count;
    }

    // Override compareTo agar objek dapat disort berdasarkan nilai friendship
    public int compareTo(ElemenFasilkom other) {

        // Jika nilai friendship objek ini lebih besar daripada objek yang dibandingkan, return -1 (karena diurutkan dari yang terbesar)
        if(this.getFriendship() > other.getFriendship()) {
            return -1;

        // Jika nilai friendship kedua objek sama, maka yang dibandingkan adalah nama kedua objek (menggunakan compareTo milik class String)
        } else if(this.getFriendship() == other.getFriendship()) {
            return this.getNama().compareTo(other.getNama());
        }

        // Jika nilai friendship objek ini lebih kecil daripada objek yang dibandingkan, return 1 (karena diurutkan dari yang terbesar)
        return 1;
    }
}