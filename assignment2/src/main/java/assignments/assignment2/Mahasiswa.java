package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;

        // Mengubah npm ke bentuk string
        String stringNPM = String.valueOf(npm);

        if (stringNPM.substring(2,4).equals("01")) {    // Mendapatkan kode jurusan dengan method substring
            this.jurusan = "IK";    // Jika kode jurusan "01", maka jurusannya adalah IK
        } else {
            this.jurusan = "SI";    // Jika kode jurusan "02", maka jurusannya adalah SI
        }
    }

    // Setter
    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNPM(long npm) {
        this.npm = npm;
    }

    public void setTotalSKS(int totalSKS) {
        this.totalSKS = totalSKS;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public void setMataKuliah(MataKuliah[] mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public void setMasalahIRS(String[] masalahIRS) {
        this.masalahIRS = masalahIRS;
    }

    // Getter
    public String getNama() {
        return this.nama;
    }

    public long getNPM() {
        return this.npm;
    }

    public int getTotalSKS() {
        return this.totalSKS;
    }

    public String getJurusan() {
        return this.jurusan;
    }

    public MataKuliah[] getMataKuliah() {
        return this.mataKuliah;
    }

    public String[] getMasalahIRS() {
        return this.masalahIRS;
    }
    
    // Method untuk mengecek apakah mahasiswa masih bisa melakukan add mata kuliah (masih kurang dari 10 mata kuliah) atau tidak 
    public boolean hitungMatkul() {
        int counterNull = 0;

        // Loop yang akan menghitung jumlah null di dalam array mataKuliah
        for(int i = 0; i < this.mataKuliah.length; i++) {
            if (this.mataKuliah[i] == null) {
                counterNull++;
            }
        }

        if (counterNull > 0) {  
            return true;    // Jika masih ada null (ada tempat kosong pada array), maka akan direturn true
        } else {
            return false;   // Jika tidak ada null (array sudah penuh), maka akan direturn false
        }
    }

    // Method untuk mengecek apakah mahasiswa sudah mengambil mata kuliah pada parameter atau belum
    public boolean cekMatkul(MataKuliah mataKuliah) {   // Menerima parameter berupa objek mata kuliah yang ingin dicek
        for(int i = 0; i < this.mataKuliah.length; i++) {
            if (this.mataKuliah[i] == mataKuliah) {
                return false;   // Jika objek mata kuliah sudah ada di dalam array (sudah diambil mahasiswa), akan direturn false
            } 
        } 
        return true;    // Jika belum ada, akan direturn true
    }

    // Method untuk mengecek apakah mata kuliah yang ingin ditambahkan kapasitasnya sudah penuh atau belum
    public boolean cekKapasitas(MataKuliah mataKuliah) {
        if (mataKuliah.getCounterKapasitas() > 0) {
            return true;    // Jika masih ada tempat kosong, maka akan direturn true
        } else {
            return false;   // Jika sudah penuh, akan direturn false
        }
    }

    // Method yang digunakan untuk mengecek jika mahasiswa belum mengambil mata kuliah sama sekali
    public boolean cekBelumAdaMatkul() {
        int counterNull = 0;

        // Loop yang akan menghitung jumlah null di dalam array mataKuliah
        for(int i = 0; i < this.mataKuliah.length; i++) {
            if (this.mataKuliah[i] == null) {
                counterNull++;
            }
        }

        if (counterNull == 10) { 
            return true;    // Jika counter null bernilai 10, maka mahasiswa belum mengambil mata kuliah sama sekali sehingga direturn true
        } else {
            return false;
        }
    }

    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan menambahkan objek mata kuliah ke array mataKuliah
        for (int i = 0; i < this.mataKuliah.length; i++) {
            if (this.mataKuliah[i] == null) {
                this.mataKuliah[i] = mataKuliah;
                this.totalSKS += mataKuliah.getSKS();   // Menambahkan totalSKS dengan SKS mata kuliah yang ditambahkan
                break;
            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan menghapus objek mata kuliah dari array mataKuliah
        for (int i = 0; i < this.mataKuliah.length; i++) {

            // Memastikan objek yang sedang diiterasi sama dengan objek mata kuliah yang ingin dihapus
            if (this.mataKuliah[i] == mataKuliah) {

                // Jika array mataKuliah penuh (objek terakhir tidak bernilai null), maka block kode ini akan dijalankan
                if (this.mataKuliah[this.mataKuliah.length-1] != null) {

                    /* Loop yang akan menggeser objek mata kuliah pada array ke kiri, dimulai dari objek yang berada pada index objek mata kuliah yang ingin didrop
                     sehingga objek mata kuliah yang ingin didrop tertimpa dengan objek mata kuliah di samping kanannya */
                    for (int j = i; j < this.mataKuliah.length - 1; j++) {
                        this.mataKuliah[j] = this.mataKuliah[j+1];
                    }

                    // Mengganti objek pada index terakhir dengan null (karena ada 1 mata kuliah yang di drop)
                    this.mataKuliah[this.mataKuliah.length-1] = null;

                // Jika array mataKuliah tidak penuh, maka block kode ini akan dijalankan
                } else {

                    // Loop yang akan menggeser objek mata kuliah pada array ke kiri
                    for (int j = i; j < this.mataKuliah.length - 1; j++) {
                        this.mataKuliah[j] = this.mataKuliah[j+1];
                    }
                }

                // totalSKS dikurangi dengan SKS mata kuliah yang di drop
                this.totalSKS -= mataKuliah.getSKS();
            }
        }
    }

    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */

        // Inisialisasi array masalahIRS dengan panjang 19
        this.masalahIRS = new String[19];

        // Validasi total SKS yang diambil

        // Jika SKS lebih dari 24, block kode ini akan dijalankan
        if (this.totalSKS > 24) {

            // Loop yang akan menambahkan string masalah IRS ke array masalahIRS
            for (int i = 0; i < this.masalahIRS.length; i++) {
                if (this.masalahIRS[i] == null) {
                    this.masalahIRS[i] = "SKS yang Anda ambil lebih dari 24";   // Menambahkan string berikut ke array masalahIRS
                    break;
                }
            }
        } 

        // Validasi kode mata kuliah yang diambil

        // Loop yang akan mengiterasi array mata kuliah
        for (MataKuliah element : this.mataKuliah) {

            // Memastikan objek yang diiterasi bukan null
            if (element != null) {

                // Jika kode mata kuliah yang sedang diiterasi bukan CS dan tidak sama dengan kode jurusan, maka block kode ini akan dijalankan
                if (!element.getKode().equals("CS") & !element.getKode().equals(this.jurusan)) {

                    // Loop yang akan menambahkan string masalah IRS ke array masalahIRS
                    for (int i = 0; i < this.masalahIRS.length; i++) {
                        if (this.masalahIRS[i] == null) {
                            this.masalahIRS[i] = String.format("Mata Kuliah %s tidak dapat diambil jurusan %s", element.getNama(), this.jurusan);   // Menambahkan string berikut ke array masalahIRS
                            break;
                        }
                    }
                }
            }
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.getNama();  // Mengembalikan nama mahasiswa
    }

}

