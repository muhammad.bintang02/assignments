package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private int counterKapasitas;
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];    // Inisialisasi array daftarMahasiswa yang panjangnya sesuai dengan kapasitas
        this.counterKapasitas = kapasitas;  // Atribut yang menyimpan kapasitas mata kuliah yang tersisa
    }

    // Setter
    public void setKode(String kode) {
        this.kode = kode;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setSKS(int sks) {
        this.sks = sks;
    }

    public void setKapasitas(int kapasitas) {
        this.kapasitas = kapasitas;
    }

    public void setDaftarMahasiswa(Mahasiswa[] daftarMahasiswa) {
        this.daftarMahasiswa = daftarMahasiswa;
    }

    // Getter
    public String getKode() {
        return this.kode;
    }

    public String getNama() {
        return this.nama;
    }

    public int getSKS() {
        return this.sks;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    public int getCounterKapasitas() {
        return this.counterKapasitas;
    }

    // Method untuk menambahkan objek mahasiswa ke array daftarMahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan menambahkan objek mahasiswa ke array daftarMahasiswa
        for (int i = 0; i < this.daftarMahasiswa.length; i++) {
            if (this.daftarMahasiswa[i] == null) {
                this.daftarMahasiswa[i] = mahasiswa;    // Jika objek yang sedang diiterasi bernilai null, maka objek tersebut akan digantikan dengan objek mahasiswa
                this.counterKapasitas -= 1; // Kapasitas mata kuliah berkurang 1 setelah objek mahasiswa ditambahkan
                break;  // Break agar nilai null lainnya tidak ikut diganti
            }
        }
    }

    // Method untuk menghapus objek mahasiswa dari array daftarMahasiswa
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */

        // Loop yang akan menghapus objek mahasiswa dari array daftarMahasiswa
        for (int i = 0; i < this.daftarMahasiswa.length; i++) {

            // Memastikan objek yang sedang diiterasi sama dengan objek mahasiswa yang ingin dihapus
            if (this.daftarMahasiswa[i] == mahasiswa) { 
                
                // Jika array daftarMahasiswa penuh (objek terakhir tidak bernilai null), maka block kode ini akan dijalankan
                if (this.daftarMahasiswa[this.daftarMahasiswa.length-1] != null) {
                    
                    /* Loop yang akan menggeser objek mahasiswa pada array ke kiri, dimulai dari objek yang berada pada index objek mahasiswa yang ingin didrop
                     sehingga objek mahasiswa yang ingin didrop tertimpa dengan objek mahasiswa di samping kanannya */
                    for (int j = i; j < this.daftarMahasiswa.length - 1; j++) {
                        this.daftarMahasiswa[j] = this.daftarMahasiswa[j+1];
                    }
                    
                    // Mengganti objek pada index terakhir dengan null (karena ada 1 mahasiswa yang di drop)
                    this.daftarMahasiswa[this.daftarMahasiswa.length-1] = null;

                // Jika array daftarMahasiswa tidak penuh, maka block kode ini akan dijalankan
                } else {

                    // Loop yang akan menggeser objek mahasiswa pada array ke kiri
                    for (int j = i; j < this.daftarMahasiswa.length - 1; j++) {
                        this.daftarMahasiswa[j] = this.daftarMahasiswa[j+1];
                    }
                }

                // Kapasitas yang tersisa bertambah 1 setelah drop dilakukan
                this.counterKapasitas += 1; 
            }
        }
    }

    // Method untuk menghitung jumlah mahasiswa yang mengambil mata kuliah 
    public int cekJumlahMahasiswa(Mahasiswa[] daftarMahasiswa) {
        int count = 0;

        // Loop yang akan menghitung objek mahasiswa yang tidak null di dalam array daftarMahasiswa
        for (Mahasiswa element : daftarMahasiswa) {
            if (element != null) {
                count++;
            }
        }
        return count;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.getNama();  // Mengembalikan nama mata kuliah
    }
}
