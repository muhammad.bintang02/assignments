package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        /* TODO: Implementasikan kode Anda di sini */

        // Loop yang mengiterasi objek mahasiswa pada array daftarMahasiswa
        for(Mahasiswa element : daftarMahasiswa) {

            // Jika  objek yang diiterasi memiliki npm yang sama dengan parameter, maka elemen tersebut akan direturn
            if(element.getNPM() == npm) {
                return element;
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        /* TODO: Implementasikan kode Anda di sini */

        // Loop yang mengiterasi objek mata kuliah pada array daftarMataKuliah
        for(MataKuliah element : daftarMataKuliah) {

            // Jika  objek yang diiterasi memiliki nama mata kuliah yang sama dengan parameter, maka elemen tersebut akan direturn
            if(element.getNama().equals(namaMataKuliah)) {
                return element;
            }
        }
        return null;
    }

    // Method untuk mendapatkan jurusan mahasiswa dalam bentuk lengkap (bukan kode)
    private String getJurusanLengkap(long npm) {    // Menerima parameter berupa npm

        // Mendapatkan objek mahasiswa berdasarkan parameter npm
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Jika objek mahasiswa memiliki jurusan IK, akan direturn string Ilmu Komputer
        if (mahasiswa.getJurusan().equals("IK")) {  
            return "Ilmu Komputer";

        // Jika objek mahasiswa memiliki jurusan SI, akan direturn string Sistem Informasi
        } else {
            return "Sistem Informasi";
        }
    }

    // Method untuk mengecek apakah array mataKuliah pada objek mahasiswa isinya null semua (belum ada matkul yang diambil) atau tidak
    private Boolean cekArrayMataKuliah(long npm) {

        // Mendapatkan objek mahasiswa berdasarkan parameter npm
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Mendapatkan array mataKuliah dengan getter milik objek mahasiswa
        MataKuliah[] mataKuliah = mahasiswa.getMataKuliah();

        // Loop yang mengiterasi objek pada array mataKuliah
        for (int i = 0; i < mataKuliah.length; i++) {
            if(mataKuliah[i] != null) {
                return true;    // Jika ada elemen yang tidak null, akan direturn true
            }
        }
        return false;   // Jika semua objeknya adalah null, maka akan direturn false
    }

    // Method untuk mengecek apakah array masalahIRS pada objek mahasiswa isinya null semua (tidak ada masalah IRS) atau tidak
    private Boolean cekArrayMasalahIRS(long npm) {

        // Mendapatkan objek mahasiswa berdasarkan parameter npm
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Mendapatkan array masalahIRS dengan getter milik objek mahasiswa
        String[] masalahIRS = mahasiswa.getMasalahIRS();

        // Loop yang mengiterasi objek pada array masalahIRS
        for (int i = 0; i < masalahIRS.length; i++) {
            if(masalahIRS[i] != null) {
                return true;
            }
        }
        return false;
    }

    // Method untuk mengecek apakah array daftarMahasiswa pada objek mata kuliah isinya null semua (belum ada mahasiswa yang mengambil) atau tidak
    private Boolean cekArrayDaftarMahasiswa(String namaMataKuliah) {

        // Mendapatkan objek mata kuliah berdasarkan parameter namaMataKuliah
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

        // Mendapatkan array daftarMahasiswa dengan getter milik objek mata kuliah
        Mahasiswa[] daftarMahasiswa = mataKuliah.getDaftarMahasiswa();

        // Loop yang mengiterasi objek pada array daftarMahasiswa
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if(daftarMahasiswa[i] != null) {
                return true;
            }
        }
        return false;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila banyaknya matkul yang diambil mahasiswa sudah 9*/
        
        // Mendapatkan objek mahasiswa berdasarkan parameter npm
        Mahasiswa mahasiswa = getMahasiswa(npm);

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            /* TODO: Implementasikan kode Anda di sini */

            // Mendapatkan objek mata kuliah berdasarkan parameter namaMataKuliah
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

            // Jika mahasiswa yang melakukan add matkul jumlah matkulnya masih kurang dari 10, belum pernah mengambil matkul ini, dan kapasitas matkulnya belum penuh, maka block kode ini akan dijalankan
            if (mahasiswa.hitungMatkul() && mahasiswa.cekMatkul(mataKuliah) && mahasiswa.cekKapasitas(mataKuliah)) {

                // Menambahkan objek mata kuliah ke array mataKuliah menggunakan method  addMatkul yang dimiliki objek mahasiswa
                mahasiswa.addMatkul(mataKuliah);

                // Menambahkan objek mahasiswa ke array daftarMahasiswa menggunakan method  addMahasiswa yang dimiliki objek mata kuliah
                mataKuliah.addMahasiswa(mahasiswa);

            // Jika mata kuliah sudah pernah diambil, string berikut akan dicetak
            } else if (mahasiswa.cekMatkul(mataKuliah) == false) {
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya.\n", mataKuliah.getNama());

            // Jika mata kuliah yang ingin diadd sudah penuh kapasitasnya, string berikut akan dicetak
            } else if (mahasiswa.cekKapasitas(mataKuliah) == false) {
                System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.\n", mataKuliah.getNama());
            
            // Jika mahasiswa sudah mengambil 10 mata kuliah, string berikut akan dicetak
            } else {
                System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
            }

        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

       /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila mahasiswa belum mengambil mata kuliah*/
       
       // Mendapatkan objek mahasiswa berdasarkan parameter npm
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Jika mahasiswa yang melakukan drop belum ada mengambil mata kuliah, maka string berikut akan dicetak
        if(mahasiswa.cekBelumAdaMatkul()) {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");

        // Jika sudah ada, maka block kode ini akan dijalankan
        } else {
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                /* TODO: Implementasikan kode Anda di sini */

                // Mendapatkan objek mata kuliah berdasarkan parameter namaMataKuliah
                MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

                // Jika mata kuliah yang ingin didrop sudah pernah diambil, maka kode ini akan dijalankan
                if (mahasiswa.cekMatkul(mataKuliah) == false) {

                    // Menghapus objek mata kuliah dari array mataKuliah menggunakan method dropMatkul yang dimiliki objek mahasiswa
                    mahasiswa.dropMatkul(mataKuliah);

                    // Menghapus objek mahasiswa dari array daftarMahasiswa menggunakan method dropMahasiswa yang dimiliki objek mata kuliah
                    mataKuliah.dropMahasiswa(mahasiswa);
                
                // Jika mata kuliah yang ingin didrop belum pernah diambil, maka string berikut akan dicetak
                } else {
                    System.out.printf("[DITOLAK] %s belum pernah diambil.\n", mataKuliah.getNama());
                }
            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        
        // Mendapatkan objek mahasiswa berdasarkan parameter npm
        Mahasiswa mahasiswa = getMahasiswa(npm);
        
        // Mendapatkan nama mahasiswa dengan getter yang dimiliki objek mahasiswa
        String nama = mahasiswa.getNama();

        // Mendapatkan jurusan mahasiswa dengan method getJurusanLengkap
        String jurusan = getJurusanLengkap(npm);

        // Mendapatkan total sks mahasiswa dengan getter yang dimiliki objek mahasiswa
        int totalSKS = mahasiswa.getTotalSKS();

        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + nama);
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + jurusan);
        System.out.println("Daftar Mata Kuliah: ");
        
        /* TODO: Cetak daftar mata kuliah 
        Handle kasus jika belum ada mata kuliah yang diambil*/
        
        // Jika array mataKuliah berisi null semua (belum ada matkul yang diambil), maka string berikut akan dicetak
        if(!cekArrayMataKuliah(npm)) {
            System.out.println("Belum ada mata kuliah yang diambil");

        // Jika sudah ada matkul yang diambil, maka block kode ini akan dijalankan
        } else {

            // Loop yang mencetak nama mata kuliah dari objek pada array mataKuliah
            for (int i = 0; i < mahasiswa.getMataKuliah().length; i++) {    // Menggunakan getter untuk mendapatkan array mataKuliah
                if (mahasiswa.getMataKuliah()[i] != null) {
                    System.out.println(i+1 + ". " + mahasiswa.getMataKuliah()[i]);
                }
            }
        }

        System.out.println("Total SKS: " + totalSKS);
        
        System.out.println("Hasil Pengecekan IRS:");
        /* TODO: Cetak hasil cek IRS
        Handle kasus jika IRS tidak bermasalah */

        // Menjalankan method cekIRS milik objek mahasiswa
        mahasiswa.cekIRS();
        
        // Jika array masalahIRS berisi null semua (tidak ada masalah), maka string berikut akan dicetak
        if(!cekArrayMasalahIRS(npm)) {
            System.out.println("IRS tidak bermasalah.");

        // Jika ada masalah IRS, maka kode ini akan dijalankan
        } else {

            // Loop yang mencetak string masalah IRS dari objek pada array masalahIRS
            for (int i = 0; i < mahasiswa.getMasalahIRS().length; i++) {    // Menggunakan getter untuk mendapatkan array masalahIRS
                if (mahasiswa.getMasalahIRS()[i] != null) {
                    System.out.println(i+1 + ". " + mahasiswa.getMasalahIRS()[i]);  
                }
            }
        }
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();

        // Mendapatkan objek mata kuliah berdasarkan parameter namaMataKuliah
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + mataKuliah.getNama());    // Menggunakan getter untuk mendapatkan nama mata kuliah
        System.out.println("Kode: " + mataKuliah.getKode());    // Menggunakan getter untuk mendapatkan kode kuliah
        System.out.println("SKS: " + mataKuliah.getSKS());  // Menggunakan getter untuk mendapatkan sks mata kuliah
        System.out.println("Jumlah mahasiswa: " + mataKuliah.cekJumlahMahasiswa(mataKuliah.getDaftarMahasiswa()));  // Menggunakan method cekJumlahMahasiswa milik objek mata kuliah untuk menghitung jumlah mahasiswa
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());  // Menggunakan getter untuk mendapatkan kapasitas mata kuliah
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       /* TODO: Cetak hasil cek IRS
        Handle kasus jika tidak ada mahasiswa yang mengambil */

        // Jika array daftarMahasiswa berisi null semua (belum ada mahasiswa yang mengambil matkul), maka string berikut akan dicetak
        if(!cekArrayDaftarMahasiswa(namaMataKuliah)) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        
        // Jika ada mahasiswa yang mengambil matkul ini, maka block kode ini akan dijalankan
        } else {

            // Loop yang akan mencetak nama mahasiswa dari objek pada array daftarMahasiswa
            for (int i = 0; i < mataKuliah.getDaftarMahasiswa().length; i++) {  // Menggunakan getter untuk mendapatkan array daftarMahasiswa
                if (mataKuliah.getDaftarMahasiswa()[i] != null) {
                    System.out.println(i+1 + ". " + mataKuliah.getDaftarMahasiswa()[i]);
                }
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }

    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            String kode = dataMatkul[0];
            String nama = dataMatkul[1];
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */

            // Loop yang akan menambahkan objek mata kuliah ke array daftarMataKuliah
            for (int j = 0; j < daftarMataKuliah.length; j++) {
                if (daftarMataKuliah[j] == null) {
                    daftarMataKuliah[j] = new MataKuliah(kode, nama, sks, kapasitas);   // Inisialisasi objek mata kuliah
                    break;
                }
            }
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            String nama = dataMahasiswa[0];
            long npm = Long.parseLong(dataMahasiswa[1]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */

            // Loop yang akan menambahkan objek mahasiswa ke array daftarMahasiswa
            for (int j = 0; j < daftarMahasiswa.length; j++) {
                if (daftarMahasiswa[j] == null) {
                    daftarMahasiswa[j] = new Mahasiswa(nama, npm);  // Inisialisasi objek mahasiswa
                    break;
                }
            }

        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
