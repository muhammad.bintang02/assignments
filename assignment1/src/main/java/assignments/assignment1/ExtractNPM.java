package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    // Method untuk menghitung hasil kali antara 2 digit NPM
    public static int kaliDigit(String npm, int index1, int index2) {   // Menerima argumen berupa NPM (dalam bentuk string) dan 2 index digit NPM yang ingin dikalikan
        
        // Deklarasi variabel untuk menyimpan hasil kali 2 digit NPM
        int hasilKali;

        // Mendapatkan digit NPM pada index yang diinginkan dengan method charAt, kemudian mengubahnya ke bentuk int, dan mengalikannya
        hasilKali = Character.getNumericValue(npm.charAt(index1)) * Character.getNumericValue(npm.charAt(index2));

        // Mereturn hasil perkalian digit
        return hasilKali;
    }

    // Method untuk menghitung jumlah digit dari jumlah hasil kali digit NPM hingga dihasilkan digit yang berada di antara 0-9
    public static int jumlahDigit(int jumlahPerkalianDigit) {

        // Jika jumlah hasil kali digit NPM sudah berada di antara 0-9, maka akan langsung direturn
        if (jumlahPerkalianDigit >= 0 && jumlahPerkalianDigit < 10) {
            return jumlahPerkalianDigit;

        // Jika jumlah hasil kali digit >= 10, maka block kode ini akan dijalankan
        } else {

            // Deklarasi variabel untuk menyimpan hasil penjumlahan digit
            int sum = 0;

            // Loop yang akan melakukan penjumlahan digit
            while (jumlahPerkalianDigit > 0) {      // Akan berhenti ketika jumlahPerkalianDigit bernilai 0
                sum += jumlahPerkalianDigit % 10;   // Mendapatkan digit paling belakang dengan mod 10 dan menjumlahkannya dengan variabel sum
                jumlahPerkalianDigit /= 10;         // Menghilangkan digit paling belakang (karena sudah dijumlahkan) dengan membaginya dengan 10
            }

            return jumlahDigit(sum);    // Memanggil kembali fungsi jumlahDigit dengan argumen sum untuk memastikan hasil penjumlahan digit sudah berada di antara 0-9
        }
    }
    
    // Method untuk memvalidasi NPM
    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean

        // Deklarasi variabel-variabel yang akan digunakan
        String stringNPM, kodeJurusan;
        Boolean validasiPanjang, validasiJurusan, validasiUmur, validasiPerhitunganDigit;
        int tahunMasuk, tahunLahir, umur, jumlahPerkalianDigit;

        // Mengubah NPM yang diterima ke bentuk string dan menyimpannya di variabel stringNPM
        stringNPM = String.valueOf(npm);

        // Validasi panjang NPM
        if (stringNPM.length() == 14) {     
            validasiPanjang = true;         // Jika panjang NPMnya sesuai (14), variabel validasiPanjang akan memiliki nilai true
        } else {                            
            validasiPanjang = false;       // Jika tidak sesuai, maka variabel validasiPanjang akan bernilai false
        }

        // Mendapatkan kode jurusan pada NPM dengan method substring dan menyimpannya di variabel kodeJurusan
        kodeJurusan = stringNPM.substring(2,4);
    
        // Validasi kode jurusan pada NPM
        if (kodeJurusan.equals("01") || kodeJurusan.equals("02") || kodeJurusan.equals("03") || kodeJurusan.equals("11") || kodeJurusan.equals("12")) {
                validasiJurusan = true;     // Jika kode jurusan yang terdapat pada NPM sesuai, maka variabel validasiJurusan akan bernilai true
            } else {
                validasiJurusan = false;    // // Jika tidak sesuai, maka variabel validasiJurusan akan bernilai false
            }

        // Validasi umur
        tahunMasuk = Integer.parseInt("20" + stringNPM.substring(0,2));     // Mendapatkan 2 digit tahun masuk dengan method substring dan mengkonkatinasinya dengan 20 (abad 20) kemudian mengubahnya ke bentuk int
        tahunLahir = Integer.parseInt(stringNPM.substring(8,12));       // Mendapatkan tahun kelahiran dengan method substring dan mengubahnya ke bentuk int
        umur = tahunMasuk - tahunLahir;     // Menghitung umur dengan cara mengurangi tahun masuk dengan tahun lahir

        if (umur >= 15) {
            validasiUmur = true;        // Jika umurnya >= 15, maka variabel validasiUmur akan bernilai true
        } else {
            validasiUmur = false;       // Jika <15, maka variabel validasiUmur akan bernilai false
        }
        
        // Mengalikan 2 digit NPM menggunakan method kaliDigit sesuai dengan ketentuan, kemudian menjumlahkan semuanya termasuk dengan digit NPM yang berada pada posisi ke 7
        jumlahPerkalianDigit = kaliDigit(stringNPM, 0, 12) + kaliDigit(stringNPM, 1, 11) + kaliDigit(stringNPM, 2, 10)
                                + kaliDigit(stringNPM, 3, 9) + kaliDigit(stringNPM, 4, 8) + kaliDigit(stringNPM, 5, 7)
                                + Character.getNumericValue(stringNPM.charAt(6));
        
        // Validasi perhitungan digit
        if (jumlahDigit(jumlahPerkalianDigit) == Character.getNumericValue(stringNPM.charAt(13))) {
            validasiPerhitunganDigit = true;        // Jika hasil perhitungan digit sama dengan digit NPM pada posisi ke 14, maka variabel validasiPerhitunganDigit akan bernilai true
        } else {
            validasiPerhitunganDigit = false;       // Jika tidak, maka variabel validasiPerhitunganDigit akan bernilai false
        }
        
        // Validasi keseluruhan aspek NPM
        if (validasiPanjang && validasiJurusan && validasiUmur && validasiPerhitunganDigit) {
            return true;    // Jika semua variabel validasi bernilai true, maka method ini akan mereturn nilai true
        } else {
            return false;   // Jika ada variabel validasi yang bernilai false, maka method akan mereturn nilai false
        }
    }

    // Method untuk melakukan ekstraksi NPM
    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format

        // Deklarasi variabel-variabel yang akan digunakan
        String stringNPM, jurusan, tanggalLahir, hasilEkstraksi;
        int tahunMasuk;

        // Jika NPM valid, maka block kode ini akan berjalan
        if (validate(npm)) {

            // Mengubah NPM yang diterima ke bentuk string
            stringNPM = String.valueOf(npm);

            // Mendapatkan 2 digit tahun masuk dengan method substring dan mengkonkatinasinya dengan 20 (abad 20)
            tahunMasuk = Integer.parseInt("20" + stringNPM.substring(0,2));

            // Mendapatkan tanggal lahir dengan method substring dan mengkonkatinasinya dengan "-"
            tanggalLahir = stringNPM.substring(4,6) + "-" + stringNPM.substring(6,8) + "-" + stringNPM.substring(8,12);

            // Variabel jurusan memiliki nilai awal string kosong
            jurusan = "";
            
            // Switch untuk menentukan jurusan berdasarkan kode jurusan (didapatkan dengan method substring)
            switch(stringNPM.substring(2,4)) {
                case "01":
                    jurusan = "Ilmu Komputer";
                    break;
                case "02" :
                    jurusan = "Sistem Informasi";
                    break;
                case "03" :
                    jurusan = "Teknologi Informasi";
                    break;
                case "11" :
                    jurusan = "Teknik Telekomunikasi";
                    break;
                case "12" :
                    jurusan = "Teknik Elektro";
                    break;
            }
            
            // Mengkonkatinasi semua informasi yang akan dicetak 
            hasilEkstraksi = "Tahun masuk: " + tahunMasuk + "\n" + "Jurusan: " + jurusan + "\n" + "Tanggal Lahir: " + tanggalLahir;

            // Return string hasil ekstraksi NPM
            return hasilEkstraksi;

        // Jika NPM tidak valid, akan direturn string berikut
        } else {
            return "NPM tidak valid!";
        }
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // TODO: Check validate and extract NPM

            // Melakukan ekstraksi NPM dan mencetak hasilnya
            System.out.println(extract(npm));
            
        }
        input.close();
    }
}